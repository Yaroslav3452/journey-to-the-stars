﻿using TechnicalScripts;
using Ui_and_other;
using UnityEngine;
using UnityEngine.AI;

namespace Astronaut
{
    public class PlayerMovement : MonoBehaviour
    {
        public static PlayerMovement Current;
        [SerializeField] private Camera mainCamera;
        private Animator _anim;
        private static readonly int AnimationPar = Animator.StringToHash("AnimationPar");
        private Quaternion _startQuaternion;
        private NavMeshAgent _navMeshAgent;

        public void ClearWaypoints()
        {
            _navMeshAgent.ResetPath();
        }

        public void SetNavmeshDestinationManually(Vector3 pos)
        {
            _navMeshAgent.destination = pos;
        }
        public void WarpPlayerToPoint(Vector3 pos)
        {
            _navMeshAgent.Warp(pos);
        }

        private void Update()
        {
            //  if (Input.GetMouseButtonDown(0))
            if (Input.GetMouseButton(0))
            {
                if (PlanetChoosePopUpWindow.Current.IsPointerOverPopUpWindow(Input.mousePosition)) return;
                if (CanvasesManager.Current.IsAnyMenuIsOpen()) return;
                if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit))
                {
                    _anim.SetInteger(AnimationPar, 1);
                    _navMeshAgent.SetDestination(hit.point);
                    PathPointsManager.Current.RelocatePointToNewPos(hit.point);
                }
            }

            if (_navMeshAgent.velocity == Vector3.zero) _anim.SetInteger(AnimationPar, 0);
            else _anim.SetInteger(AnimationPar, 1);
        }

        private void Awake()
        {
            Current = this;
            _anim = gameObject.GetComponentInChildren<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }
    }
}
