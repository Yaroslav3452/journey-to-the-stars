﻿using UnityEngine;

namespace Astronaut
{
    public class PathPointsManager : MonoBehaviour
    {
        public static PathPointsManager Current;
        [SerializeField] GameObject point;
        private MovingPoint _movingPoint;
        private Transform _pointTransform;

        public void RelocatePointToNewPos(Vector3 position)
        {
            _pointTransform.position = position;
            point.SetActive(true);
            _movingPoint.SetBorders();
        }

        public void RemovePoint()
        {
            if (point.activeSelf == false) return;
            point.SetActive(false);
            _pointTransform.position = new Vector3(999, 999, 999);
        }

        private void Awake()
        {
            Current = this;
            _pointTransform = point.transform;
            _movingPoint = point.GetComponent<MovingPoint>();
        }
    }
}
