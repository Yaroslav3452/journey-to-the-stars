﻿using Interfaces;
using UnityEngine;

namespace Astronaut
{
    public class MovingPoint : MonoBehaviour, IObjects
    {
        [SerializeField] private Vector3 dist = new Vector3(0f, .1f, 0f);
        [SerializeField] private float speed = 0f;
        private Transform _transform;
        private Vector3 _targetPosition = Vector3.zero;
        private Vector3 _positionUpper = Vector3.zero;
        private Vector3 _positionBottom = Vector3.zero;

        public void SetBorders()
        {
            var position = _transform.position;
            _positionBottom = position - dist;
            _positionUpper = position + dist;
            _targetPosition = _positionBottom;
        }

        private void Awake()
        {
            _transform = transform; 
        }

        private void Start()
        {
            SetBorders();
        }

        private void Update()
        {
            if (_targetPosition != Vector3.zero)
            {
                _transform.position = Vector3.MoveTowards(_transform.position, _targetPosition, speed * Time.fixedDeltaTime);
                if (_targetPosition == _transform.position)
                {
                    if (_targetPosition == _positionUpper) _targetPosition = _positionBottom;
                    else _targetPosition = _positionUpper;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            PathPointsManager.Current.RemovePoint();
        }

        public ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.MovePoints;
        }

        public void ResetValuesToDefault()
        {
        }
    }
}
