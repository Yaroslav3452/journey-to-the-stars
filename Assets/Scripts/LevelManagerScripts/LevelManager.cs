﻿using System.Collections.Generic;
using ResourcesAndProduction;
using ResourcesAndProduction.ProductionBuildings;
using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.Rocket;
using UnityEngine;
// ReSharper disable ArrangeObjectCreationWhenTypeEvident

namespace LevelManagerScripts{

    public class LevelManager : MonoBehaviour
    {
        public RocketLauncher rocketLauncher;
        public List<StorageInfo> storageInfos = new List<StorageInfo>();
        public List<ProductionBuildings> productionBuildings = new List<ProductionBuildings>();
        public List<ConstructionManager> constructionManagers = new List<ConstructionManager>();
        public LevelsSwitcherSystem.SkyboxSettingsStruct skyboxSettingsSettings;
        public int levelStage = 0;
        [SerializeField] List<GameObject> stage1 = new List<GameObject>();
        [SerializeField] List<GameObject> stage2 = new List<GameObject>();
        [SerializeField] List<GameObject> stage3 = new List<GameObject>();
        [SerializeField] List<GameObject> stage4 = new List<GameObject>();


        public void SetLevelStage(int idx)
        {
            levelStage = idx;
        }

        /// <summary>
        /// set higher stage if no param, or higher stage after param
        /// </summary>
        /// <param name="idx"> param</param>
        public void CheckWhichStageNeedToBeActivated(int ?idx)
        {
            var caser = 0;
            if(idx != null) caser = (int) idx;
            else caser = levelStage;
            switch (caser)
            {
                case 0:
                    EnableStage1();
                    break;
                case 1:
                    EnableStage2();
                    break;
                case 2:
                    EnableStage3();
                    break;
                case 3:
                    EnableStage4();
                    break;
            }   
        }

        private void EnableStage1()
        {
            foreach (var go in stage1)
            {
                go.SetActive(true);
            }
        }

        private void EnableStage2()
        {
            foreach (var go in stage2)
            {
                go.SetActive(true);
            }
        }

        private void EnableStage3()
        {
            foreach (var go in stage3)
            {
                go.SetActive(true);
            }
        }

        private void EnableStage4()
        {
            foreach (var go in stage4)
            {
                go.SetActive(true);
            }
        }

        private void Awake()
        {
            storageInfos.AddRange(GetComponentsInChildren<StorageInfo>());
            productionBuildings.AddRange(GetComponentsInChildren<ProductionBuildings>());
            constructionManagers.AddRange(GetComponentsInChildren<ConstructionManager>());
            rocketLauncher = GetComponentInChildren<RocketLauncher>();
        }
    }
}