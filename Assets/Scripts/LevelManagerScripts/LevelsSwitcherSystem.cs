﻿using System;
using System.Collections.Generic;
using GraphicsScripts;
using ResourcesAndProduction.Rocket;
using Ui_and_other;
using UnityEngine;

namespace LevelManagerScripts
{
    public class LevelsSwitcherSystem : MonoBehaviour
    {
        public static LevelsSwitcherSystem Current;
        public LevelManager currentLevelManager;
        public List<LevelManager> levelManagers = new List<LevelManager>();
        [Serializable]
        public struct SkyboxSettingsStruct
        {
            public Texture noise;
            public Color lowerColor;
            public Color upperColor;
        }
        
        public int ReturnCurrentLevelIdx()
        {
            return levelManagers.IndexOf(currentLevelManager);
        }

        public void SetCurrentLevelManager(int idx)
        {
            if(idx < levelManagers.Count && idx >= 0) currentLevelManager = levelManagers[idx];
            else Debug.LogError("Out of range here", this);
        }
        public void LoadNewLocation(RocketLauncher launcher)
        {
            switch (launcher.ReturnPlanetType())
            {
                case PlanetChoosePopUpWindow.PlanetsEnum.Earth:
                    LoadEarth();
                    break;
                case PlanetChoosePopUpWindow.PlanetsEnum.Moon:
                    LoadMoon();
                    break;
                case PlanetChoosePopUpWindow.PlanetsEnum.Mars:
                    LoadMars();
                    break;
                case PlanetChoosePopUpWindow.PlanetsEnum.Alien:
                    LoadAlien();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            SkyboxColorManager.Current.SetNewNoiseColors();
        }
        private void LoadMars()
        {
            currentLevelManager = levelManagers[(int)PlanetChoosePopUpWindow.PlanetsEnum.Mars];
        }
        private void LoadEarth()
        {
            currentLevelManager = levelManagers[(int)PlanetChoosePopUpWindow.PlanetsEnum.Earth];
        }

        private void LoadMoon()
        {
            currentLevelManager = levelManagers[(int)PlanetChoosePopUpWindow.PlanetsEnum.Moon];
        }
        private void LoadAlien()
        {
            currentLevelManager = levelManagers[(int)PlanetChoosePopUpWindow.PlanetsEnum.Alien];
        }
        private void Awake()
        {
            Current = this;
        }
    }
}