using Astronaut;
using LevelManagerScripts;
using ResourcesAndProduction.Rocket;
using Ui_and_other;
using UnityEngine;
using UnityEngine.UI;

namespace PlayerTransferBetweenPlanets{

    public class PlayerTransferSystem : MonoBehaviour
    {
        public static PlayerTransferSystem Current;
        public RocketLauncher destination;
        [SerializeField] ConfirmationButton confirmation;
        [SerializeField] Button button;

        private bool SetDestinationForRelocating()
        {
            if (LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.targetDestination != null)
            {
                destination = LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.targetDestination;
                return true;
            }

            return false;
        }

        private void OpenConfirmationWindow()
        {
            confirmation.OpenMenu();
        }
        public void CloseConfirmationWindow()
        {
            confirmation.CloseMenu();
        }
        public void RelocatePlayerButtonPressed()
        {
         if(!SetDestinationForRelocating()) return;
         OpenConfirmationWindow();
        }

        public void SetInteractableForRelocateButton(bool bl)
        {
            button.interactable = bl;
        }
        private void FixedUpdate()
        {
            if (confirmation.isConfirmed)
            {
                Relocate();
                confirmation.isConfirmed = false;
            }
        }

        private void Relocate()
        {
            var position = destination.playerSpawnPoint.position;
            LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.LaunchRocket(true);
            PlayerMovement.Current.gameObject.transform.position = position;
            PlayerMovement.Current.WarpPlayerToPoint(position);
            LevelsSwitcherSystem.Current.LoadNewLocation(destination);
            destination = null;
        }
        private void Awake()
        {
            Current = this;
            button.interactable = false;
        }
    }
}