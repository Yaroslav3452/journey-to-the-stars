using Astronaut;
using ResourcesAndProduction.ResourceStorages;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class HighQualityFuel : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.Fuels.Contains(this)) PlayerResourcesStorage.Current.Fuels.Push(this);
        }

        protected override void AddResourceToDrone()
        {
            var parentDroneStorage = GetComponentInParent<StorageInfo>();
            if(parentDroneStorage == null)
            {
                Debug.LogError("droneStorage is null", this);
                return;
            }
            //todo: i dont know is needed such override :(
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.Fuel;
        }
    }
}