using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class Concrete : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.Concretes.Contains(this)) PlayerResourcesStorage.Current.Concretes.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.Concrete;
        }
    }
}