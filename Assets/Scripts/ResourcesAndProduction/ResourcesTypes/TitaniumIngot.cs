using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class TitaniumIngot : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.TitaniumIngots.Contains(this)) PlayerResourcesStorage.Current.TitaniumIngots.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.Titanium;
        }
    }
}