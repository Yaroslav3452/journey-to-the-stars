﻿using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
   public class PrefabsStorage : MonoBehaviour
   {
      public static PrefabsStorage Current;
      public Vector3 scaleForStorage = Vector3.one;
      public GameObject drone;
      public GameObject oil;
      public GameObject fuel;
      public GameObject droneParts;
      public GameObject highTechParts;
      public GameObject concrete;
      public GameObject circuit;
      public GameObject buildingParts;
      public GameObject aliveMetal;
      public GameObject titanOre;
      public GameObject titanIngot;
      public GameObject martianRegolite;
      public GameObject superMetal;
      public GameObject artifactsParts;
      public GameObject artifacts;
      public GameObject alienCircuits;
      private void Awake()
      {
         Current = this;
      }
   }
}
