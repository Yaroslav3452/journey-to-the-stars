using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class AlienCircuits : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.AlienCircuits.Contains(this)) PlayerResourcesStorage.Current.AlienCircuits.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.AlienCircuits;
        }
    }
}