using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class AliveMetal : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.AliveMetals.Contains(this)) PlayerResourcesStorage.Current.AliveMetals.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.AliveMetal;
        }
    }
}