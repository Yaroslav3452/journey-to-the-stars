using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class SuperMetal : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.SuperMetals.Contains(this)) PlayerResourcesStorage.Current.SuperMetals.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.SuperMetal;
        }
    }
}