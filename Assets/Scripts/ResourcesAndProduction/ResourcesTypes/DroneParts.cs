using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{

    public class DroneParts : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.DroneParts.Contains(this)) PlayerResourcesStorage.Current.DroneParts.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.DroneParts;
        }
    }
}