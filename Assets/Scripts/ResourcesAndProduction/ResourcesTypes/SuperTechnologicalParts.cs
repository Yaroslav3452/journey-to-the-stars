using Astronaut;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class SuperTechnologicalParts : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.SuperTechnologicalParts.Contains(this)) PlayerResourcesStorage.Current.SuperTechnologicalParts.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.HighTechParts;
        }
    }
}