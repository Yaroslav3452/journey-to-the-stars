using System.Collections.Generic;
using Astronaut;
using Interfaces;
using ResourcesAndProduction.ResourceStorages;
using UnityEngine;
// ReSharper disable ArrangeObjectCreationWhenTypeEvident

namespace ResourcesAndProduction.ResourcesTypes
{
    public class ResourceObject : MonoBehaviour, IObjects
    {
        public List<Transform> waypoints = new List<Transform>();
        public Transform positionAtStorage;
        public Transform positionAtPLayerStorage;
        //public Transform positionAtDroneStorage;
        //public bool isMovingToPlayer = false;
        public bool isNeedToRemoveAfterMoving = false;
        public Vector3 targetScale = Vector3.zero;
        [SerializeField] float objectMovingSpeed = 3f;
        [SerializeField] private float errorDelta = .05f;
        private Quaternion? _targetRotation = null;
        private Transform _transform;
        private Rigidbody _rigidbody;
        private float _defaultSpeed = 3f;
        private Vector3 _defaultScale;

        public void SetNewParentTransform(Transform parent)
        {
            transform.SetParent(parent);
        }

        public void ResetValuesToDefault()
        {
            waypoints.Clear();
            positionAtStorage = null;
            positionAtPLayerStorage = null;
            //isMovingToPlayer = false;
            isNeedToRemoveAfterMoving = false;
            objectMovingSpeed = _defaultSpeed;
            errorDelta = .05f;
            transform.localScale = _defaultScale;
            targetScale = Vector3.zero;
        }

        private void Update()
        {
            if (_targetRotation != null)
            {
                _transform.rotation = Quaternion.Slerp(_transform.rotation, (Quaternion) _targetRotation, 2 * Time.deltaTime);
                if(_transform.rotation == _targetRotation) _targetRotation = null;
            }
            if (targetScale != Vector3.zero)
            {
                Scaler();
            }
        }

        private void FixedUpdate()
        {
            if (positionAtStorage != null && waypoints.Count == 0) MoveToStorage();
            MovingByWaypoints();
        }

        private void MovingByWaypoints()
        {
            if (waypoints.Count <= 0) return;
            if (waypoints.Count >= 5) Debug.LogError("something bad here with waypoints?", this);
            MoveObjectTo(waypoints[0]);
            if ((transform.position - waypoints[0].position).magnitude <= errorDelta) //if resource is close to target
            {
                //moving on conveyor to storage
                var parentStorage = GetComponentInParent<StorageInfo>();
                if (waypoints[0] == positionAtStorage)
                {
                    parentStorage.resourceUnits.Add(gameObject);
                    positionAtStorage = null;
                }
                else if (waypoints[0] == positionAtPLayerStorage) //moving to player storage from default storage
                {
                    //isMovingToPlayer = false;
                    positionAtPLayerStorage = null;
                    _transform.localPosition = Vector3.zero;
                    _targetRotation = new Quaternion(0, 0, 0, _transform.rotation.w);
                    var playerResourcesStorage = GetComponentInParent<PlayerResourcesStorage>();
                    if(!playerResourcesStorage.resourceUnits.Contains(gameObject))
                        playerResourcesStorage.resourceUnits.Add(gameObject);
                    AddResourceToPlayerSpecificStack();
                }
                // else if (waypoints[0] == positionAtDroneStorage) //moving to drone storage from default storage
                // {
                //     _transform.localPosition = Vector3.zero;
                //     if(!parentStorage.resourceUnits.Contains(gameObject))
                //         parentStorage.resourceUnits.Add(gameObject); 
                //     positionAtDroneStorage = null;
                // }
                if (isNeedToRemoveAfterMoving)
                {
                    ObjectRemoverManager.Current.MoveObjectInReserve(gameObject);
                }
                waypoints.RemoveAt(0);
            }
        }
        private void Scaler()
        {
            var localScale = _transform.lossyScale;
            _transform.localScale = Vector3.Lerp(localScale, targetScale, 2 * Time.deltaTime);
            if (Mathf.Abs(localScale.magnitude - targetScale.magnitude) < .001f)
            {
                targetScale = Vector3.zero;
            }
        }

        private void MoveToStorage()
        {
           MoveObjectTo(positionAtStorage);  
           if((transform.position - positionAtStorage.position).magnitude <= errorDelta) EndMovingToStorage();
        }

        private void EndMovingToStorage()
        {
            positionAtStorage = null;
            _transform.localPosition = Vector3.zero;
            _targetRotation = new Quaternion(0, 0, 0, _transform.rotation.w);
        }

        /// <summary>
        /// Moving to object via lerp
        /// </summary>
        /// <param name="target">target to move towards</param>
        private void MoveObjectTo(Transform target)
        {
            var targetPos = target.position;
            var position = _transform.position;
            if (position == Vector3.zero) Debug.LogError("wtf moved to zero") ;
            var newPosition = Vector3.Slerp(position, targetPos, objectMovingSpeed * Time.fixedDeltaTime);
            //var newPosition = Vector3.MoveTowards(position, targetPos, objectMovingSpeed * Time.fixedDeltaTime);
            _rigidbody.MovePosition(newPosition);
        }
        protected virtual void AddResourceToPlayerSpecificStack()
        {
            //rewrite
        }

        protected virtual void AddResourceToDrone()
        {
            //rewrite
        }
        private void Start()
        {
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody>();
            _defaultSpeed = objectMovingSpeed;
            _defaultScale = transform.lossyScale;
        }

        public virtual ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.Nothing;
        }
    }
}