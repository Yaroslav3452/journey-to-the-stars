﻿using Astronaut;
using JetBrains.Annotations;
using UnityEngine;

namespace ResourcesAndProduction.ResourcesTypes
{
    public class Oil : ResourceObject
    {
        protected override void AddResourceToPlayerSpecificStack()
        {
            if(!PlayerResourcesStorage.Current.Oils.Contains(this)) PlayerResourcesStorage.Current.Oils.Push(this);
        }

        public override ObjectRemoverManager.ObjectType ReturnObjectType()
        {
            return ObjectRemoverManager.ObjectType.Oil;
        }
    }
}
