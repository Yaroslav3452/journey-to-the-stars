﻿using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.ResourcesTypes;
using UnityEngine;

namespace ResourcesAndProduction.ProductionBuildings
{
  public class Conveyor : MonoBehaviour
  {
    [SerializeField] private GameObject fromStorage;
    [SerializeField] private GameObject toStorage;
    [SerializeField] private Transform startOfConveyor;
    [SerializeField] private Transform endOfConveyor;
    // [SerializeField] private float speed;
    private ProductionBuildings _targetProductionBuilding;
    private StorageInfo _fromInfo;
    private StorageInfo _toInfo;
    private float _time;

    /// <summary>
    /// set waypoints for resource moving
    /// </summary>
    private void MoveUnit()
    {
      var target = _toInfo.GetPointToMove();
      if(target == null) return;
      if(_fromInfo.resourceUnits.Count == 0) return;
      GameObject resourceGo = null;
      for (int i = _fromInfo.resourceUnits.Count - 1; i >= 0; i--)
      {
        resourceGo = _fromInfo.resourceUnits[i];
        if(resourceGo.GetComponent<ResourceObject>().waypoints.Count == 0) break;
      }

      if(resourceGo == null) return;
      var resource = resourceGo.GetComponent<ResourceObject>();
      resource.waypoints.Add(startOfConveyor);
      resource.waypoints.Add(endOfConveyor);
      resource.positionAtStorage = target;
      resource.waypoints.Add(target);
      resource.transform.parent = target;
      _fromInfo.resourceUnits.Remove(resourceGo);
    }

    private void Start()
    {
      _fromInfo = fromStorage.GetComponentInChildren<StorageInfo>();
      _toInfo = toStorage.GetComponentInChildren<StorageInfo>();
      _targetProductionBuilding = toStorage.GetComponentInParent<ProductionBuildings>();
    }

    private void FixedUpdate()
    {
      _time += Time.fixedDeltaTime;
      if (_time >= _targetProductionBuilding.timePerUnit / _targetProductionBuilding.price)
      {
        if(_toInfo.resourceUnits.Count < _toInfo.spawnPoints.Count)
        {
          MoveUnit();
          _time = 0;
        }
      }
    }
  }
}
