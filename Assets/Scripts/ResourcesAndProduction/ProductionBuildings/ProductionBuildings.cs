﻿using System;
using System.Collections.Generic;
using Interfaces;
using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.ResourcesTypes;
using UnityEngine;

namespace ResourcesAndProduction.ProductionBuildings
{
    public class ProductionBuildings : MonoBehaviour
    {
        public float timePerUnit = 10;
        public int price = 3;
        public List<ObjectRemoverManager.ObjectType> consumableResourceType = new List<ObjectRemoverManager.ObjectType>();
        public StorageInfo storageInfoOut;
        public StorageInfo storageInfoIn;
        [NonSerialized] public ObjectRemoverManager.ObjectType Product;
        [SerializeField] private GameObject resourcePrefab;
        [SerializeField] private Transform spawnPointForResource;
        [SerializeField] private Transform pointForConsumableResources;
        [SerializeField] private float time;

        /// <summary>
        /// products resource at storage and manage resource consuming
        /// </summary>
        public void ProductResource()
        {
            if(storageInfoOut == null) return;
            var targetTransform = storageInfoOut.GetPointToMove();  //check for free space on storages
            if (targetTransform == null) return;
            if (consumableResourceType.Count == 0) SpawnResource(targetTransform); //if no consumable resources
            if (consumableResourceType.Count == 1)
            {
                if (storageInfoIn.ReturnCountOfThisResource(consumableResourceType[0]) >= price)
                {
                    SpawnResource(targetTransform);
                    storageInfoIn.RemoveResourceFromStorageViaMovingToConsumer(price, pointForConsumableResources,
                        consumableResourceType[0], null);
                }
            }
            else if(consumableResourceType.Count == 2 &&
                    storageInfoIn.ReturnCountOfThisResource(consumableResourceType[0]) >= price &&
                    storageInfoIn.ReturnCountOfThisResource(consumableResourceType[1]) >= price) 
            {
                SpawnResource(targetTransform);
                storageInfoIn.RemoveResourceFromStorageViaMovingToConsumer(price, pointForConsumableResources,
                    consumableResourceType[0], consumableResourceType[1]);
            }
        }

        /// <summary>
        /// Return count of theoretical products taking into account free space and raw resources 
        /// </summary>
        /// <returns>count of theoretical products</returns>
        public int ReturnCountOfNewProducts()
        {
            var storageOutFreeSpace = storageInfoOut.spawnPoints.Count - storageInfoOut.resourceUnits.Count;
            if(storageInfoIn != null)
            {
                if (storageInfoIn.resourceUnits.Count <= price * consumableResourceType.Count) return 0;
                if (storageInfoIn.resourceUnits.Count / (price * consumableResourceType.Count) > storageOutFreeSpace) 
                    return storageOutFreeSpace; 
                return storageInfoIn.resourceUnits.Count;
            }
            return storageOutFreeSpace;
        }
        private void SpawnResource(Transform targetTransform)
        {
            if(targetTransform == null) return;
            var resource = ObjectRemoverManager.Current.ReUseObject(resourcePrefab, spawnPointForResource.position, Quaternion.identity);
            resource.transform.parent = targetTransform;
            var script = resource.GetComponent<ResourceObject>();
            script.positionAtStorage = targetTransform;
            storageInfoOut.resourceUnits.Add(resource); 
        }

   
        private void FixedUpdate()
        {
            time += Time.fixedDeltaTime;
            if (time >= timePerUnit)
            {
                ProductResource();
                time = 0;
            }
        }

        private void Start()
        {
            Product = resourcePrefab.GetComponent<IObjects>().ReturnObjectType();
        }
    }
}
