﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ResourcesAndProduction.ResourceStorages
{
   public class SetPointsForResourcesStoraging : MonoBehaviour
   {
      public List<Transform> spawnPoints = new List<Transform>();
      public bool isGenerating = false;
      public bool delete;
      [SerializeField] private float additionalYindent;
      [SerializeField] private Transform leftTopPosition;
      [SerializeField] private Transform rightBottomPosition;
      [SerializeField] private GameObject resource;
      [SerializeField] private GameObject spawnPointPrefab;
      [SerializeField] private float indention = 0.1f;
      private Vector3 _resourceSize;

      private void Start()
      {
         if(resource) _resourceSize = resource.transform.localScale;
      }

      private void CreateSpawnPoints()
      {
         var xcount = 0;
         var zcount = 0;
         var isEnd = false;
         var indentX = _resourceSize.x + indention;
         var indentY = _resourceSize.y + additionalYindent;
         var indentZ = _resourceSize.z + indention;
         var border = rightBottomPosition.transform.position;
         Vector3 position;
         //
         spawnPoints.Add(leftTopPosition);
         position = leftTopPosition.position;
         CreateSpawnPoint(position);
         //
         while (!isEnd)
         {
            if ((position.x + indentX) < border.x)
            {
               position.x += indentX;
               CreateSpawnPoint(position);
               continue;
            }
            if (xcount == 0) xcount = spawnPoints.Count;
            var firstSpawnPointInPreviousLine = spawnPoints[spawnPoints.Count - xcount].transform.position;
            if (firstSpawnPointInPreviousLine.z - indentZ > border.z)
            {
               position = firstSpawnPointInPreviousLine;
               position.z -= indentZ;
               CreateSpawnPoint(position);
               continue;
            }
            if (zcount == 0) zcount = spawnPoints.Count / xcount;
            var firsSpawnPointInThisLayer = spawnPoints[spawnPoints.Count - xcount * zcount].transform.position;
            if (firsSpawnPointInThisLayer.y + indentY < border.y)
            {
               position = firsSpawnPointInThisLayer;
               position.y += indentY;
               CreateSpawnPoint(position);
               continue;
            }
            isEnd = true;
            isGenerating = false;
         }
      }
   
      private void CreateSpawnPoint(Vector3 pos)
      {
         if (spawnPoints[spawnPoints.Count - 1].position == pos) return;
         var temp = Instantiate(spawnPointPrefab, pos, Quaternion.identity);
         spawnPoints.Add(temp.transform);
      }

      private void FixedUpdate()
      {
         if(isGenerating)
         {
            CreateSpawnPoints();
         }

         if (delete)
         {
            foreach (var spawnPoint in spawnPoints.ToList())
            {
               if(spawnPoint != leftTopPosition) Destroy(spawnPoint.gameObject);
            }
            spawnPoints.Clear();
            delete = false;
         }
      }
   }
}
