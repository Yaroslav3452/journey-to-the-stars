using System.Collections.Generic;
using ResourcesAndProduction.ResourcesTypes;

namespace ResourcesAndProduction.ResourceStorages
{
    public class ImportStorage : StorageInfo
    {
        public Stack<Oil> Oils = new Stack<Oil>();
        public Stack<HighQualityFuel> Fuels = new Stack<HighQualityFuel>();
        public Stack<SimpleCircuit> SimpleCircuits = new Stack<SimpleCircuit>();
        public Stack<BuildingParts> BuildingsParts = new Stack<BuildingParts>();
        public Stack<DroneParts> DroneParts = new Stack<DroneParts>();
        //
        public Stack<Concrete> Concretes = new Stack<Concrete>();
        public Stack<TitaniumOre> TitaniumOres = new Stack<TitaniumOre>();
        public Stack<TitaniumIngot> TitaniumIngots = new Stack<TitaniumIngot>();
        //
        public Stack<MartianRegolite> MartianRegolites = new Stack<MartianRegolite>();
        public Stack<SuperMetal> SuperMetals = new Stack<SuperMetal>();
        public Stack<SuperTechnologicalParts> SuperTechnologicalParts = new Stack<SuperTechnologicalParts>();
        public Stack<ArtifactParts> ArtifactParts  = new Stack<ArtifactParts>();
        //
        public Stack<Artifacts> Artifacts = new Stack<Artifacts>();
        public Stack<AliveMetal> AliveMetals = new Stack<AliveMetal>();
        public Stack<AlienCircuits> AlienCircuits = new Stack<AlienCircuits>();
    }
}