﻿using System.Collections.Generic;
using Astronaut;
using CameraScripts;
using Interfaces;
using UnityEngine;

namespace ResourcesAndProduction.Rocket
{
   public class RocketController : MonoBehaviour, IObjects
   {
      public Vector3 targetScale = Vector3.zero;
      public Vector3 defaultScale;
      [SerializeField] private bool isRocketLaunching = false;
      [SerializeField] private float maxSpeed = 0;
      [SerializeField] private float acceleration = 0;
      [SerializeField] private List<ParticleSystem> engineFireParticles = new List<ParticleSystem>();
      [SerializeField] private float targetHeight = 0;
      private Transform _transform;
      private float _currentSpeed = 1;
      private int _scalingSpeed;

      public void SetCurrentScaleForRocket(Vector3 scale)
      {
         _transform.localScale = scale;
      }
      public void SetTargetScaleForRocket(Vector3 targetScale)
      {
         this.targetScale = targetScale;
      }

      public void LaunchRocket()
      {
         isRocketLaunching = true;
         PlayRocketAnimations();
      }

      private void Update()
      {
         if (isRocketLaunching)
         {
            if(_currentSpeed <= maxSpeed) _currentSpeed *= acceleration; 
            transform.Translate(Vector3.up * (_currentSpeed * Time.deltaTime));
            if (_transform.position.y >= targetHeight)
            {
              RemoveRocketToReserve();
            }
         }

         if (targetScale != Vector3.zero)
         {
            _scalingSpeed = 2;
            _transform.localScale = Vector3.Lerp(_transform.localScale, targetScale, _scalingSpeed * Time.deltaTime);
            if(_transform.localScale == defaultScale) targetScale = Vector3.zero;
         }
      }

      private void RemoveRocketToReserve()
      {
         isRocketLaunching = false;
         StopParticleAnimations();
         CameraController.Current.RemoveObjectFromTargetGroup(gameObject.transform);
         CameraController.Current.SetCameraFollowObject(PlayerMovement.Current.gameObject.transform);
         var parentRocketLauncher = GetComponentInParent<RocketLauncher>();
         if (parentRocketLauncher != null) parentRocketLauncher.CreateNewRocket();
         ObjectRemoverManager.Current.MoveObjectInReserve(gameObject);
      }
      private void Awake()
      {
         _transform = transform;
         defaultScale = _transform.localScale;
         engineFireParticles.AddRange(GetComponentsInChildren<ParticleSystem>());
      }
      private void PlayRocketAnimations()
      {
         foreach (var particle in engineFireParticles)
         {
            particle.Play();
         }
      }

      private void StopParticleAnimations()
      {
         foreach (var particle in engineFireParticles)
         {
            particle.Stop();
         }
      }

      public void ResetValuesToDefault()
      {
         _transform.localScale = Vector3.zero;
         _transform.localPosition = Vector3.zero;
         StopParticleAnimations();
      }

      public ObjectRemoverManager.ObjectType ReturnObjectType()
      {
         return ObjectRemoverManager.ObjectType.Rocket;
      }
   }
}
