﻿using System.Collections.Generic;
using CameraScripts;
using ResourcesAndProduction.ResourceStorages;
using ResourceTransfer;
using Ui_and_other;
using UnityEngine;
// ReSharper disable RedundantDefaultMemberInitializer

namespace ResourcesAndProduction.Rocket
{
   public class RocketLauncher : MonoBehaviour
   {
      public StorageInfo storageInfoIn;
      public ImportStorage importStorage;
      public List<ObjectRemoverManager.ObjectType> exported = new List<ObjectRemoverManager.ObjectType>();
      public List<ObjectRemoverManager.ObjectType> imported = new List<ObjectRemoverManager.ObjectType>();
      public Transform resourceSpawnPoint;
      public RocketController currentRocketController;
      public RocketLauncher targetDestination = null;
      public bool needLaunch = false;
      public bool receiveResourcesToRocket = true;
      public Transform playerSpawnPoint;
      [SerializeField] private PlanetChoosePopUpWindow.PlanetsEnum typeOfPlanet;
      [SerializeField] private Transform rocketSpawnPlace;
      [SerializeField] private GameObject rocketPrefab;
      private float _time;
      private const float LaunchDelay = 3f;

      public void LaunchRocket(bool isNeedToLookAtLaunch)
      {
         if(isNeedToLookAtLaunch) CameraController.Current.LookAtRocketLaunch();
         currentRocketController.LaunchRocket();
         currentRocketController = null;
         needLaunch = false;
         receiveResourcesToRocket = true;
         _time = 0f;
         ResourceTransferSystem.Current.SpawnResourcesAtTargetRocketLauncherImportStorage(targetDestination);
      }
      /// <summary>
      /// creates new rocket at rocketLauncher
      /// </summary>
      public void CreateNewRocket()
      {
         var rocket =
            ObjectRemoverManager.Current.ReUseObject(rocketPrefab, rocketSpawnPlace.position, Quaternion.identity);
         rocket.transform.parent = rocketSpawnPlace;
         currentRocketController = rocket.GetComponent<RocketController>();
         currentRocketController.SetTargetScaleForRocket(currentRocketController.defaultScale);
         currentRocketController.SetCurrentScaleForRocket(Vector3.zero);
      }

      public PlanetChoosePopUpWindow.PlanetsEnum ReturnPlanetType()
      {
         return typeOfPlanet;
      }
      private void SendResourcesFromStorageToRocket()
      {
         if (storageInfoIn.resourceUnits.Count == 0)
         {
            needLaunch = false;
            return;
         }
         ResourceTransferSystem.Current.AddResourcesToGlobalStorage(storageInfoIn.resourceUnits, targetDestination);
         storageInfoIn.RemoveResourceFromStorageViaMovingToConsumer(rocketSpawnPlace, targetDestination.imported);
         receiveResourcesToRocket = false;
      }

      private void FixedUpdate()
      {
         if (needLaunch)
         {
            if(currentRocketController == null) return;
            if(receiveResourcesToRocket) SendResourcesFromStorageToRocket();
            _time += Time.fixedDeltaTime;
            if (_time >= LaunchDelay)
            {
               LaunchRocket(false);
            }
         }
      }
      private void Start()
      {
         storageInfoIn = GetComponentInChildren<StorageInfo>();
         currentRocketController = GetComponentInChildren<RocketController>();
      }
   }
}
