﻿using ResourcesAndProduction.ResourceStorages;
using Ui_and_other;
using UnityEngine;
// ReSharper disable RedundantDefaultMemberInitializer

namespace ResourcesAndProduction
{
  public class ConstructionManager : MonoBehaviour
  {
    public StorageInfo resourcesSource;
    public int oilNeeded;
    public int fuelNeeded;
    public int simpleCircuitsNeeded;
    public int dronePartsNeeded;
    public int buildingPartsNeeded;
    public int superTechnologicalPartsNeeded;
    public int titaniumOreNeeded;
    public int titaniumIngotNeeded;
    public int concreteNeeded;
    public int martianRegoliteNeeded;
    public int superMetalNeeded;
    public int alienArtifactPartsNeeded;
    public int aliveMetalNeeded;
    public int alienArtifactsNeeded;
    public int alienCircuitsNeeded;
    public bool builded = false;
    [SerializeField] private GameObject model;
    [SerializeField] private float buildSpeed = 2f;
    private GameObject _storageOut;
    private ResourceLabel _resourceLabel;

    private void CheckForConstruction()
    {
      if (resourcesSource == null) return;
      if (oilNeeded == 0 && fuelNeeded == 0 &&
          simpleCircuitsNeeded == 0 &&
          dronePartsNeeded == 0 &&
          buildingPartsNeeded == 0 &&
          superTechnologicalPartsNeeded == 0 &&
          titaniumOreNeeded == 0 &&
          titaniumIngotNeeded == 0 &&
          concreteNeeded == 0 &&
          martianRegoliteNeeded == 0 &&
          superMetalNeeded == 0 &&
          alienArtifactPartsNeeded == 0 &&
          aliveMetalNeeded == 0 &&
          alienArtifactsNeeded == 0 &&
          alienCircuitsNeeded == 0)
      {
        ConstructBuilding();
      }
    }

    public void InstantConstruction()
    {
      if(_resourceLabel != null) _resourceLabel.SmoothDisappear();
      builded = true;
      if (model != null)
      {
        model.SetActive(true);
        model.transform.localScale = Vector3.one;
      }
      if(_storageOut != null)
      {
        _storageOut.SetActive(true);
        _storageOut.transform.localScale = Vector3.one;
      }
      else
      {
        resourcesSource.getResources = false;
        resourcesSource.giveResources = true;
      }
    }

    private void ConstructBuilding()
    {
      if (_resourceLabel != null) _resourceLabel.SmoothDisappear();
      TakeResourcesFromStorages();
      builded = true;
      if (model != null)
      {
        model.SetActive(true);
        model.transform.localScale = Vector3.zero;
      }

      if (_storageOut != null)
      {
        _storageOut.SetActive(true);
        _storageOut.transform.localScale = Vector3.zero;
      }

      if (_storageOut == null)
      {
        resourcesSource.giveResources = true;
        resourcesSource.getResources = false;
      }
    }

    private void TakeResourcesFromStorages()
    {
      if (resourcesSource != null)
        resourcesSource.RemoveResourceFromStorageViaMovingToConsumer(model.transform);
    }
    private void Update()
    {
      if (builded)
      {
        var targetScale = Vector3.Lerp(model.transform.localScale, Vector3.one, buildSpeed * Time.deltaTime);
        model.transform.localScale = targetScale;
        if ((Vector3.one - model.transform.localScale).magnitude <= new Vector3(.01f,.01f,.01f).magnitude)
        {
          if (resourcesSource)
          {
            resourcesSource.getResources = true;
            resourcesSource.giveResources = false;
          }
          this.enabled = false;
        }
        if(_storageOut != null) _storageOut.transform.localScale = targetScale;
      }
      else
      {
        CheckForConstruction();
      }
    }

    private void CalcForResourcesAtStorages()
    {
      oilNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Oil);
      fuelNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Fuel);
      simpleCircuitsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Circuits);
      dronePartsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.DroneParts);
      buildingPartsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.BuildingParts);
      superTechnologicalPartsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.HighTechParts);
      titaniumOreNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.TitaniumOre);
      titaniumIngotNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Titanium);
      concreteNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Concrete);
      martianRegoliteNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.MartianRegolite);
      superMetalNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.SuperMetal);
      alienArtifactPartsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.ArtifactParts);
      aliveMetalNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.AliveMetal);
      alienArtifactsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Artifacts);
      alienCircuitsNeeded -= resourcesSource.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.AlienCircuits);
    }

    private void Awake()
    {
      _resourceLabel = GetComponentInChildren<ResourceLabel>();
      var storages = GetComponentsInChildren<StorageInfo>();
      foreach (var storage in storages)
      {
        if(storage == resourcesSource) continue;
        _storageOut = storage.gameObject;
      }
    }

    private void Start()
    {
      CalcForResourcesAtStorages();
    }
  }
}
