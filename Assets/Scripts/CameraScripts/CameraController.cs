﻿using System;
using System.Collections.Generic;
using Cinemachine;
using LevelManagerScripts;
using UnityEngine;

namespace CameraScripts
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Current;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        [SerializeField] private CinemachineTargetGroup targetGroup;
        [SerializeField] private List<MemberSettings> memberSettingsList = new List<MemberSettings>();

        [Serializable]
        public struct MemberSettings
        {
            public float targetWeight;
            public float targetRadius;
            public Transform member;

            public MemberSettings(float targetWeight, float targetRadius, Transform member)
            {
                this.targetWeight = targetWeight;
                this.targetRadius = targetRadius;
                this.member = member;
            }
        }

        public void SetTargetGroupWeightForObject(GameObject gObject, float weight, float radius)
        {
            var idx = targetGroup.FindMember(gObject.transform);
       
            targetGroup.m_Targets[idx].weight = weight;
            targetGroup.m_Targets[idx].radius = radius;
        }

        public void LookAtRocketLaunch()
        {
            SetCameraFollowObject(LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.currentRocketController.transform);
            AddObjectToTargetGroup(LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.currentRocketController.transform, 1000, 100);
        }

        public void RemoveObjectFromTargetGroup(Transform gObj)
        {
            targetGroup.RemoveMember(gObj.transform);
        }
        public void AddObjectToTargetGroup(Transform gObj, float weight, float radius)
        {
            var member = new MemberSettings(weight, radius, gObj);
            memberSettingsList.Add(member);
            targetGroup.AddMember(gObj.transform, 0, radius);
        }

        public void SetCameraFollowObject(Transform target)
        {
            virtualCamera.Follow = target;
        }

        private void FixedUpdate()
        {
            foreach (var targetGroupMTarget in targetGroup.m_Targets)
            {
                foreach (var memberAtList in memberSettingsList)
                {
                    if (memberAtList.member == targetGroupMTarget.target)
                    {
                        var newWeight = targetGroupMTarget.weight + (memberAtList.targetWeight - targetGroupMTarget.weight) * 0.01f;
                        var newRadius = targetGroupMTarget.radius + (memberAtList.targetRadius - targetGroupMTarget.radius) * 0.01f;
                        SetTargetGroupWeightForObject(memberAtList.member.gameObject, newWeight, newRadius);
                    }
                }
            }
        }
        
        private void SetAllMembersWeightToZero()
        {
            for (int i = 0; i < targetGroup.m_Targets.Length; i++)
            {
                targetGroup.m_Targets[i].weight = 0;
                targetGroup.m_Targets[i].radius = 0;
            }
        }

        private void Awake()
        {
            Current = this;
            targetGroup = GetComponent<CinemachineTargetGroup>();
        }
    }
}
