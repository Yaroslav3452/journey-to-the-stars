﻿using System.Collections.Generic;
using Interfaces;
using UnityEngine;
// ReSharper disable FieldCanBeMadeReadOnly.Local
//because i dont want to make them readonly, RIDER!!!!!!!!
// ReSharper disable Unity.RedundantSerializeFieldAttribute
// ReSharper disable ArrangeObjectCreationWhenTypeEvident

public class ObjectRemoverManager : MonoBehaviour
{
    public static ObjectRemoverManager Current;

    //earth
    // ReSharper disable once Unity.RedundantSerializeFieldAttribute
    [SerializeField] private Stack<GameObject> _oil = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _highQualityOil = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _simpleCircuits = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _droneParts = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _buildingParts = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _superTechnologicalParts = new Stack<GameObject>();

    //moon
    [SerializeField] private Stack<GameObject> _titaniumOre = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _titaniumIngot = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _concrete = new Stack<GameObject>();

    //mars
    [SerializeField] private Stack<GameObject> _martianRegolite = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _superMetal = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _alienArtifactParts = new Stack<GameObject>();

    //alienWorld
    [SerializeField] private Stack<GameObject> _aliveMetal = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _alienArtifact = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _alienCircuits = new Stack<GameObject>();

    //
    [SerializeField] private Stack<GameObject> _rockets = new Stack<GameObject>();
    [SerializeField] private Stack<GameObject> _movePoints = new Stack<GameObject>();
    private readonly Vector3 _pointToMove = new Vector3(9999, 9999, 9999);

    /// <summary>
    /// Object types(int) for reUsing.
    /// </summary>
    public enum ObjectType
    {
        Nothing = 0,
        Oil = 1,
        Fuel,
        Circuits,
        DroneParts,
        BuildingParts,
        HighTechParts,
        TitaniumOre,
        Titanium,
        Concrete,
        MartianRegolite,
        SuperMetal,
        ArtifactParts,
        AliveMetal,
        Artifacts,
        AlienCircuits,
        Rocket,
        MovePoints
    }

    /// <summary>
    /// Moves object in reserve point and disable it
    /// </summary>
    /// <param name="movableObject">object move to</param>
    public void MoveObjectInReserve(GameObject movableObject)
    {
        movableObject.transform.position = _pointToMove;
        movableObject.transform.parent = transform;
        var stack = ReturnNeededList(movableObject.GetComponent<IObjects>().ReturnObjectType());
        stack.Push(movableObject);
        movableObject.SetActive(false);
    }

    /// <summary>
    /// give object from reserve or create new
    /// </summary>
    /// <param name="prefab">etalon</param>
    /// <param name="position"> position where u need to spawn object</param>
    /// <param name="quaternion"> quaternion of object</param>
    /// <returns>gameObject of new instance or reused</returns>
    public GameObject ReUseObject(GameObject prefab, Vector3 position, Quaternion quaternion)
    {
        GameObject temp = null;
        var type = prefab.GetComponent<IObjects>().ReturnObjectType();
        var stack = ReturnNeededList(type);
        if (stack == null)
        {
            Debug.LogError("exception stack == null", this);
            return null;
        }

        if (stack.Count > 0)
        {
            temp = stack.Pop();
            temp.SetActive(true);
            ResetValuesForObject(temp);
            temp.name = "reused";
            temp.transform.rotation = quaternion;
            if (temp.transform.localScale == Vector3.zero)
            {
                temp.name = "SomethingWrongWithScale";
                Debug.LogError(
                    "something wrong here with reusing" + "Object: " + temp.name + " parent " +
                    temp.transform.parent.name, this);
            }
        }
        else
        {
            temp = Instantiate(prefab, position, quaternion);
        }

        temp.transform.position = position;
        return temp;
    }

    private Stack<GameObject> ReturnNeededList(ObjectType type)
    {
        switch (type)
        {
            case ObjectType.Oil:
                return _oil;
            case ObjectType.Fuel:
                return _highQualityOil;
            case ObjectType.DroneParts:
                return _droneParts;
            case ObjectType.BuildingParts:
                return _buildingParts;
            case ObjectType.HighTechParts:
                return _superTechnologicalParts;
            case ObjectType.Circuits:
                return _simpleCircuits;
            case ObjectType.TitaniumOre:
                return _titaniumOre;
            case ObjectType.Titanium:
                return _titaniumIngot;
            case ObjectType.Concrete:
                return _concrete;
            case ObjectType.MartianRegolite:
                return _martianRegolite;
            case ObjectType.SuperMetal:
                return _superMetal;
            case ObjectType.ArtifactParts:
                return _alienArtifactParts;
            case ObjectType.Artifacts:
                return _alienArtifact;
            case ObjectType.AlienCircuits:
                return _alienCircuits;
            case ObjectType.AliveMetal:
                return _aliveMetal;
            case ObjectType.Rocket:
                return _rockets;
            case ObjectType.MovePoints:
                return _movePoints;
            default:
                return null;
        }
    }

    private static void ResetValuesForObject(GameObject obj)
    {
        if (obj.TryGetComponent<IObjects>(out var script))
        {
            script.ResetValuesToDefault();
        }
    }

    private void Awake()
    {
        Current = this;
    }
}
