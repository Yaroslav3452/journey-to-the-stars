namespace Interfaces
{
    public interface IObjects
    {
        ObjectRemoverManager.ObjectType ReturnObjectType();
        void ResetValuesToDefault();
    }
}