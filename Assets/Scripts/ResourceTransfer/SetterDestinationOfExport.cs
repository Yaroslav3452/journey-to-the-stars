using System.Collections.Generic;
using PlayerTransferBetweenPlanets;
using ResourcesAndProduction.Rocket;
using TMPro;
using Ui_and_other;
using UnityEngine;

namespace ResourceTransfer
{
    public class SetterDestinationOfExport : ButtonPhysical
    {
        [SerializeField] private bool isLauncher;
        [SerializeField] private bool isChoosingDestination = false;
        [SerializeField] private GameObject buttonsParent;
        [SerializeField] private List<SetterDestinationOfExport> otherButtons = new List<SetterDestinationOfExport>();
        private RocketLauncher _parentLauncher;
        private TextMeshPro _textMeshPro;
        private Color _targetColorForText = Color.black;
        [SerializeField] private float timer;
        private bool _playerStay = false;

        protected override void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _playerStay = true;
                if (isChoosingDestination)
                {
                    PlanetChoosePopUpWindow.Current.OpenMenu();
                }
                pressed = true;
                TargetColor = pressedColor;
                _targetColorForText = Color.red;
                SetDefaultPosForOtherButtons();
                MoveButton();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                if (isLauncher)
                {
                    if (_parentLauncher.needLaunch || !_parentLauncher.receiveResourcesToRocket ||
                        _parentLauncher.currentRocketController == null || _parentLauncher.targetDestination == null
                        || _parentLauncher.storageInfoIn.resourceUnits.Count == 0)
                    {
                        _textMeshPro.text = "LAUNCH 0%";
                        timer = 0f;
                    } else _textMeshPro.text = "LAUNCH " + (int)(timer / 3f * 100) + "%";
                    if (timer <= 3f) return;
                    if (_parentLauncher.targetDestination == null) return;
                    LaunchRocket();
                    timer = 0f;
                }
            }
        }

        protected override void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _playerStay = false;
                if (isChoosingDestination)
                {
                    PlanetChoosePopUpWindow.Current.CloseMenu();
                    PlayerTransferSystem.Current.CloseConfirmationWindow();
                }

                if (isLauncher)
                {
                    _textMeshPro.text = "LAUNCH 0%";
                }

                if (isChoosingDestination)
                {
                    if (_parentLauncher.targetDestination != null)
                        _textMeshPro.text = "Destination - " + _parentLauncher.targetDestination.name;
                    else _textMeshPro.text = "choose Destination ";
                }
                pressed = false;
                _targetColorForText = Color.white;
                TargetColor = DefaultColor;
                TargetPos = DefaultPos;
                MoveButton();
                timer = 0f;
            }
        }

        private void SetDefaultPosForOtherButtons()
        {
            foreach (var button in otherButtons)
            {
                button.SetDefaults();
                button._targetColorForText = Color.white;
            }
        }

        private void SetDefaults()
        {
            TargetPos = DefaultPos;
            pressed = false;
            TargetColor = DefaultColor;
            _targetColorForText = Color.white;
        }

        protected new void FixedUpdate()
        {
            if (_playerStay) timer += Time.fixedDeltaTime;
            if (TargetPos != Vector3.zero)
            {
                var pos = Vector3.Lerp(Transform.localPosition, TargetPos, AnimationSpeed * Time.deltaTime);
                Transform.localPosition = pos;
                MeshRenderer.material.color = Color.Lerp(MeshRenderer.material.color, TargetColor, AnimationSpeed * Time.deltaTime);
                if (TargetPos == Transform.position) TargetPos = Vector3.zero;
            }

            if (_targetColorForText != Color.black)
            {
                _textMeshPro.color = Color.Lerp(_textMeshPro.color, _targetColorForText, AnimationSpeed * Time.deltaTime);
                if (_textMeshPro.color == _targetColorForText) _targetColorForText = Color.black;
            }
        }

        private void LaunchRocket()
        {
            if(_parentLauncher.currentRocketController != null && _parentLauncher.storageInfoIn.resourceUnits.Count > 0)
                _parentLauncher.needLaunch = true;
        }
        private void Awake()
        {
            var parent = transform.parent;
            otherButtons.AddRange(buttonsParent.GetComponentsInChildren<SetterDestinationOfExport>());
            otherButtons.Remove(this);
            _textMeshPro = parent.GetComponentInChildren<TextMeshPro>();
            _parentLauncher = GetComponentInParent<RocketLauncher>();
        }
    }
}