﻿using System;
using UnityEngine;

namespace Ui_and_other
{
    public class ButtonPhysical : MonoBehaviour
    {
        public bool pressed = false;
        [SerializeField] protected Color pressedColor;
        [SerializeField] protected Vector3 pressedPos;
        protected Vector3 DefaultPos = Vector3.zero;
        protected Transform Transform;
        protected Vector3 TargetPos = Vector3.zero;
        protected Color TargetColor;
        protected Color DefaultColor;
        protected MeshRenderer MeshRenderer;
        protected const float AnimationSpeed = 3f;

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                pressed = true;
                TargetColor = pressedColor;
                MoveButton();
            }
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                pressed = false;
                TargetColor = DefaultColor;
                MoveButton();
            }
        }

        protected void MoveButton()
        {
            if (pressed)
            {
                TargetPos = pressedPos;
            }
            else
            {
                TargetPos = DefaultPos;
            }
        }
        protected void FixedUpdate()
        {
            if(TargetPos != Vector3.zero)
            {
                var pos = Vector3.Lerp(Transform.localPosition, TargetPos, AnimationSpeed * Time.deltaTime);
                Transform.localPosition = pos;
                MeshRenderer.material.color = Color.Lerp(MeshRenderer.material.color, TargetColor, AnimationSpeed * Time.deltaTime);
                if (TargetPos == Transform.position) TargetPos = Vector3.zero;
            }
        }

        private void Start()
        {
            Transform = transform;
            DefaultPos = Transform.localPosition;
            pressedPos = new Vector3(0, .1f, 0);
            MeshRenderer = GetComponent<MeshRenderer>();
            DefaultColor = MeshRenderer.material.color;
        }
    }
}
