﻿using System;
using System.Collections.Generic;
using Interfaces;
using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.ResourcesTypes;
using ResourcesAndProduction.Rocket;
using Ui_and_other;
using UnityEngine;
// ReSharper disable RedundantDefaultMemberInitializer

namespace ResourceTransfer
{
    public class ResourceTransferSystem : MonoBehaviour
    {
        public static ResourceTransferSystem Current;
        public RocketLauncher earthLauncher;
        public RocketLauncher moonLauncher;
        public RocketLauncher marsLauncher;
        public RocketLauncher alienLauncher;
        [SerializeField] private float timerForCheckAndSpawn = 0f;
        [SerializeField] private LauncherResourceReserve earth = new LauncherResourceReserve();
        [SerializeField] private LauncherResourceReserve moon = new LauncherResourceReserve();
        [SerializeField] private LauncherResourceReserve mars = new LauncherResourceReserve();
        [SerializeField] private LauncherResourceReserve alien = new LauncherResourceReserve();
        [Serializable]
        public struct LauncherResourceReserve
        {
            public int oil;
            public int fuel;
            public int droneParts;
            public int buildingsPart;
            public int circuits;
            public int highTechParts;
            public int titanOre;
            public int titanIngot;
            public int concrete;
            public int martianRegolite;
            public int superMetal;
            public int artifactsParts;
            public int aliveMetal;
            public int alienArtifacts;
            public int alienCircuits;
            public LauncherResourceReserve(int oil, int fuel, int droneParts, int buildingsPart, int circuits,
                int highTechParts, int titanOre, int titanIngot, int concrete, int martianRegolite, int superMetal,
                int artifactsParts, int aliveMetal, int alienArtifacts, int alienCircuits)
            {
                this.oil = oil;
                this.fuel = fuel;
                this.droneParts = droneParts;
                this.buildingsPart = buildingsPart;
                this.circuits = circuits;
                this.highTechParts = highTechParts;
                this.titanOre = titanOre;
                this.titanIngot = titanIngot;
                this.concrete = concrete;
                this.martianRegolite = martianRegolite;
                this.superMetal = superMetal;
                this.artifactsParts = artifactsParts;
                this.aliveMetal = aliveMetal;
                this.alienArtifacts = alienArtifacts;
                this.alienCircuits = alienCircuits;
            }
        }

        public RocketLauncher ReturnNeededRocketLauncherByType(PlanetChoosePopUpWindow.PlanetsEnum type)
        {
            switch (type)
            {
                case PlanetChoosePopUpWindow.PlanetsEnum.Earth:
                    return earthLauncher;
                case PlanetChoosePopUpWindow.PlanetsEnum.Moon:
                    return moonLauncher;
                case PlanetChoosePopUpWindow.PlanetsEnum.Mars:
                    return marsLauncher;
                case PlanetChoosePopUpWindow.PlanetsEnum.Alien:
                    return alienLauncher;
                default:
                    return null;
            }
        }
/// <summary>
/// Adds resources to planet's reserve
/// </summary>
/// <param name="list">list of resources</param>
/// <param name="targetLauncher">target planet's launcher</param>
/// <exception cref="ArgumentOutOfRangeException">wrong resource type</exception>
        public void AddResourcesToGlobalStorage(List<GameObject> list, RocketLauncher targetLauncher)
        {
            ref var neededStruct = ref ReturnNeededStruct(targetLauncher);
            foreach (var unit in list)
            {
                var objectType = unit.GetComponent<IObjects>().ReturnObjectType();
                if(!targetLauncher.imported.Contains(objectType)) continue;
                switch (objectType)
                {
                    case ObjectRemoverManager.ObjectType.Oil:
                        neededStruct.oil++;
                        break;
                    case ObjectRemoverManager.ObjectType.Fuel:
                        neededStruct.fuel++;
                        break;
                    case ObjectRemoverManager.ObjectType.DroneParts:
                        neededStruct.droneParts++;
                        break;
                    case ObjectRemoverManager.ObjectType.BuildingParts:
                        neededStruct.buildingsPart++;
                        break;
                    case ObjectRemoverManager.ObjectType.HighTechParts:
                        neededStruct.highTechParts++;
                        break;
                    case ObjectRemoverManager.ObjectType.Circuits:
                        neededStruct.circuits++;
                        break;
                    case ObjectRemoverManager.ObjectType.TitaniumOre:
                        neededStruct.titanOre++;
                        break;
                    case ObjectRemoverManager.ObjectType.Titanium:
                        neededStruct.titanIngot++;
                        break;
                    case ObjectRemoverManager.ObjectType.Concrete:
                        neededStruct.concrete++;
                        break;
                    case ObjectRemoverManager.ObjectType.MartianRegolite:
                        neededStruct.martianRegolite++;
                        break;
                    case ObjectRemoverManager.ObjectType.SuperMetal:
                        neededStruct.superMetal++;
                        break;
                    case ObjectRemoverManager.ObjectType.ArtifactParts:
                        neededStruct.artifactsParts++;
                        break;
                    case ObjectRemoverManager.ObjectType.Artifacts:
                        neededStruct.alienArtifacts++;
                        break;
                    case ObjectRemoverManager.ObjectType.AlienCircuits:
                        neededStruct.alienCircuits++;
                        break;
                    case ObjectRemoverManager.ObjectType.AliveMetal:
                        neededStruct.aliveMetal++;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void SpawnResourcesAtTargetRocketLauncherImportStorage(RocketLauncher rocketLauncher)
        {
            var storageInfo = rocketLauncher.importStorage;
            var resourceSpawnPoint = rocketLauncher.resourceSpawnPoint;
            ref var tempStruct = ref ReturnNeededStruct(rocketLauncher);
            var oilTemp = tempStruct.oil;
            var fuelTemp = tempStruct.fuel;
            var dronePartsTemp = tempStruct.droneParts;
            var buildingsPartTemp = tempStruct.buildingsPart;
            var circuitsTemp = tempStruct.circuits;
            var highTechPartsTemp = tempStruct.highTechParts;
            var titanOreTemp = tempStruct.titanOre;
            var titanIngotTemp = tempStruct.titanIngot;
            var concreteTemp = tempStruct.concrete;
            var martianRegoliteTemp = tempStruct.martianRegolite;
            var superMetalTemp = tempStruct.superMetal;
            var artifactsPartsTemp = tempStruct.artifactsParts;
            var aliveMetalTemp = tempStruct.aliveMetal;
            var alienArtifactsTemp = tempStruct.alienArtifacts;
            var alienCircuitsTemp = tempStruct.alienCircuits;
            for (var i = 0; i < oilTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.oil, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.Oils.Push(obj.GetComponent<Oil>());
                tempStruct.oil--;
            }
            for (var i = 0; i < fuelTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.fuel, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.Fuels.Push(obj.GetComponent<HighQualityFuel>());
                tempStruct.fuel--;
            }
            for (var i = 0; i < dronePartsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.droneParts, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.DroneParts.Push(obj.GetComponent<DroneParts>());
                tempStruct.droneParts--;
            }
            for (var i = 0; i < buildingsPartTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.buildingParts, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.BuildingsParts.Push(obj.GetComponent<BuildingParts>());
                tempStruct.buildingsPart--;
            }
            for (var i = 0; i < circuitsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.circuit, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.SimpleCircuits.Push(obj.GetComponent<SimpleCircuit>());
                tempStruct.circuits--;
            }
            for (var i = 0; i < highTechPartsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.highTechParts, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.SuperTechnologicalParts.Push(obj.GetComponent<SuperTechnologicalParts>());
                tempStruct.highTechParts--;
            }
            for (var i = 0; i < titanOreTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.titanOre, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.TitaniumOres.Push(obj.GetComponent<TitaniumOre>());
                tempStruct.titanOre--;
            }
            for (var i = 0; i < titanIngotTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.titanIngot, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.TitaniumIngots.Push(obj.GetComponent<TitaniumIngot>());
                tempStruct.titanIngot--;
            }  
            for (var i = 0; i < martianRegoliteTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.martianRegolite, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.MartianRegolites.Push(obj.GetComponent<MartianRegolite>());
                tempStruct.martianRegolite--;
            }
            for (var i = 0; i < concreteTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.concrete, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.Concretes.Push(obj.GetComponent<Concrete>());
                tempStruct.concrete--;
            }
            for (var i = 0; i < superMetalTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.superMetal, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.SuperMetals.Push(obj.GetComponent<SuperMetal>());
                tempStruct.superMetal--;
            }
            for (var i = 0; i < artifactsPartsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.artifactsParts, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.ArtifactParts.Push(obj.GetComponent<ArtifactParts>());
                tempStruct.artifactsParts--;
            }
            for (var i = 0; i < alienArtifactsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.artifacts, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.Artifacts.Push(obj.GetComponent<Artifacts>());
                tempStruct.alienArtifacts--;
            }
            for (var i = 0; i < aliveMetalTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.aliveMetal, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.AliveMetals.Push(obj.GetComponent<AliveMetal>());
                tempStruct.aliveMetal--;
            }
            for (var i = 0; i < alienCircuitsTemp; i++)
            {
                var obj =
                    SpawnResource(PrefabsStorage.Current.alienCircuits, storageInfo, resourceSpawnPoint, Quaternion.identity);
                if (obj == null) return;
                storageInfo.AlienCircuits.Push(obj.GetComponent<AlienCircuits>());
                tempStruct.alienCircuits--;
            }
        }

        private GameObject SpawnResource(GameObject prefab, ImportStorage storageInfo, Transform spawnPoint,
            Quaternion quaternion)
        {
            var targetTransform = storageInfo.GetPointToMove();
            if (targetTransform == null) return null;
            var resource = ObjectRemoverManager.Current.ReUseObject(prefab, spawnPoint.position, quaternion);
            resource.TryGetComponent<ResourceObject>(out var script);
            script.SetNewParentTransform(targetTransform);
            script.positionAtStorage = targetTransform;
            storageInfo.resourceUnits.Add(resource.gameObject);
            script.targetScale = PrefabsStorage.Current.scaleForStorage;
            //Debug.Log(resource.name);
            return resource;
        }

        private ref LauncherResourceReserve ReturnNeededStruct(RocketLauncher rocketLauncher)
        {
            switch (rocketLauncher.ReturnPlanetType())
            {
                case PlanetChoosePopUpWindow.PlanetsEnum.Earth:
                    return ref earth;
                case PlanetChoosePopUpWindow.PlanetsEnum.Moon:
                    return ref moon;
                case PlanetChoosePopUpWindow.PlanetsEnum.Mars:
                    return ref mars;
                case PlanetChoosePopUpWindow.PlanetsEnum.Alien:
                    return ref alien;
            }

            throw new InvalidOperationException("null struct");
        }

        private void FixedUpdate()
        {
            timerForCheckAndSpawn += Time.fixedDeltaTime;
            if (timerForCheckAndSpawn <= 5f) return;
            timerForCheckAndSpawn = 0f;
            SpawnResourcesAtTargetRocketLauncherImportStorage(earthLauncher);
            SpawnResourcesAtTargetRocketLauncherImportStorage(moonLauncher);
            SpawnResourcesAtTargetRocketLauncherImportStorage(marsLauncher);
            SpawnResourcesAtTargetRocketLauncherImportStorage(alienLauncher);

        }
        private void Awake()
        {
            Current = this;
        }
    }
}