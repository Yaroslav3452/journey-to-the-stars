﻿using System;
using System.Collections.Generic;
using ResourcesAndProduction;
using ResourcesAndProduction.ProductionBuildings;
using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.ResourcesTypes;
using ResourcesAndProduction.Rocket;
using UnityEngine;
using UnityEngine.AI;

namespace ResourceTransfer.Drones
{

    public class Drone : MonoBehaviour
    {
        public DroneStation parentDroneStation;
        [SerializeField] private bool forceMoveToDroneStation;
        [SerializeField] private Transform currentTarget;
        [SerializeField] private ProductionBuildings targetProductionBuildingToDelivery;
        [SerializeField] private RocketLauncher targetRocketLauncher;
        [SerializeField] private float landingSpeed = 2f;
        [SerializeField] private GameObject model;
        [SerializeField] Vector3 targetPosAtDroneStation;
        private StorageInfo _droneStorageInfo;
        private Transform _transform;
        private NavMeshAgent _navMeshAgent;
        private Vector3 _defaultModelPosition;
        private readonly List<ParticleSystem> _particleSystems = new List<ParticleSystem>();

        public void SendDroneToDroneStation(bool isForceMoving)
        {
            forceMoveToDroneStation = isForceMoving;
            var target = parentDroneStation.GetPointForDock(this);
            if (target != null)
            {
                var position = target.position;
                targetPosAtDroneStation = position;
                MoveDroneTo(position);
            }
        }

        private void MoveDroneTo(Vector3 target)
        {
            if(!_navMeshAgent.isOnNavMesh) return;
            SetEngineParticleState(true);
            _navMeshAgent.SetDestination(target);
        }

        private void FindTaskForThisDrone()
        {
            if (_droneStorageInfo.resourceUnits.Count > 0 && targetProductionBuildingToDelivery == null)
            {
                FindImplementationForResourcesAtDrone();
                return;
            }

            if (targetProductionBuildingToDelivery == null && targetRocketLauncher == null)
            {
                FindTargetsForDelivery();
            }

            if (currentTarget == null && targetProductionBuildingToDelivery == null && targetRocketLauncher == null)
                SendDroneToDroneStation(false);
        }

        private void FindImplementationForResourcesAtDrone()
        {
            foreach (var resource in _droneStorageInfo.resourceUnits)
            {
                var objectType = resource.GetComponent<ResourceObject>().ReturnObjectType();
                foreach (var productionBuildings in
                         parentDroneStation.planetParentLevelManager.productionBuildings)
                {
                    if (productionBuildings.GetComponent<ConstructionManager>().builded == false) continue;
                    if (CheckIsThisResourceNeededForProduction(objectType, productionBuildings))
                    {
                        targetProductionBuildingToDelivery = productionBuildings;
                        return;
                    }
                }

                foreach (var exportType in parentDroneStation.planetParentLevelManager.rocketLauncher.exported)
                {
                    if (objectType == exportType)
                    {
                        targetRocketLauncher = parentDroneStation.planetParentLevelManager.rocketLauncher;
                    }
                }
            }
        }

        private void FindTargetsForDelivery()
        {
            if (parentDroneStation.planetParentLevelManager == null) return;
            foreach (var storage in parentDroneStation.planetParentLevelManager.storageInfos)
            {
                if (storage.resourceUnits.Count <= 0) continue;
                if (storage.giveResources == false) continue;
                foreach (var resourceUnit in storage.resourceUnits)
                {
                    var objectType = resourceUnit.GetComponent<ResourceObject>().ReturnObjectType();
                    foreach (var productionBuilding in parentDroneStation.planetParentLevelManager.productionBuildings)
                    {
                        if (productionBuilding.storageInfoIn == null) continue;
                        if (productionBuilding.storageInfoIn.GetPointToMove() == null) continue;
                        if (productionBuilding.GetComponent<ConstructionManager>().builded == false) continue;
                        if (CheckIsThisResourceNeededForProduction(objectType, productionBuilding))
                        {
                            var transform1 = storage.transform;
                            currentTarget = transform1;
                            targetProductionBuildingToDelivery = productionBuilding;
                            return;
                        }
                    }

                    foreach (var exportType in parentDroneStation.planetParentLevelManager.rocketLauncher.exported)
                    {
                        if (objectType == exportType)
                        {
                            var transform1 = storage.transform;
                            currentTarget = transform1;
                            targetRocketLauncher = parentDroneStation.planetParentLevelManager.rocketLauncher;
                        }
                    }
                }
            }
        }

        private void GiveResourcesToStorage()
        {
            if (targetProductionBuildingToDelivery != null)
            {
                InitiateMovingResourceToStorageFromDrone(targetProductionBuildingToDelivery.storageInfoIn);
                targetProductionBuildingToDelivery = null;
            }
            if (targetRocketLauncher != null)
            {
                InitiateMovingResourceToStorageFromDrone(targetRocketLauncher.storageInfoIn);
                targetRocketLauncher = null;
            }
        }

        private void GetResourcesToDrone()
        {
            if (currentTarget == null) return;
            currentTarget.TryGetComponent<StorageInfo>(out var storage);
            if (storage != null && storage.giveResources)
            {
                InitiateMovingResourcesToDrone(storage);
                currentTarget = null;
            }
        }

        private void InitiateMovingResourcesToDrone(StorageInfo storageInfo)
        {
            for (var i = 0; i < 2; i++)
            {
                var targetPos = _droneStorageInfo.GetPointToMove();
                if (targetPos == null) return;
                GameObject resourceGameObject = null;
                ResourceObject resourceObject = null;
                for (int j = storageInfo.resourceUnits.Count - 1; j >= 0; j--)
                {
                    var resourceUnit = storageInfo.resourceUnits[j];
                    resourceObject = resourceUnit.GetComponent<ResourceObject>();
                    if (targetProductionBuildingToDelivery != null &&
                        CheckIsThisResourceNeededForProduction(resourceObject.ReturnObjectType(),
                            targetProductionBuildingToDelivery))
                    {
                        resourceGameObject = resourceUnit;
                        break;
                    }
                    if (targetRocketLauncher != null)
                    {
                        resourceGameObject = resourceUnit;
                        break;
                    }
                }

                if (resourceGameObject == null)
                {
                    currentTarget = null;
                    targetProductionBuildingToDelivery = null;
                    return;
                }

                storageInfo.resourceUnits.Remove(resourceGameObject);
                resourceObject.ResetValuesToDefault();
                resourceObject.waypoints.Add(targetPos);
                resourceObject.positionAtStorage = targetPos;
                resourceObject.transform.parent = targetPos;
            }

            currentTarget = null;
        }

        private void InitiateMovingResourceToStorageFromDrone(StorageInfo storageInfo)
        {
            if (_droneStorageInfo.resourceUnits.Count == 0) return;
            for (int i = 0; i < 2; i++)
            {
                var targetTransform = storageInfo.GetPointToMove();
                if (targetTransform == null) return;
                ResourceObject element = null;
                foreach (var resourceUnit in _droneStorageInfo.resourceUnits)
                {
                    element = resourceUnit.GetComponent<ResourceObject>();
                    if (targetProductionBuildingToDelivery != null &&
                        !CheckIsThisResourceNeededForProduction(element.ReturnObjectType(), targetProductionBuildingToDelivery))
                    {
                        element = null;
                    }
                }

                if (element == null) return;
                _droneStorageInfo.resourceUnits.Remove(element.gameObject);
                element.positionAtStorage = null;
                element.waypoints.Clear();
                element.positionAtStorage = targetTransform;
                element.transform.parent = targetTransform;
                element.targetScale = PrefabsStorage.Current.scaleForStorage;
                if (targetProductionBuildingToDelivery != null)
                    targetProductionBuildingToDelivery.storageInfoIn.resourceUnits.Add(element.gameObject);
                if (targetRocketLauncher != null)
                    targetRocketLauncher.storageInfoIn.resourceUnits.Add(element.gameObject);
            }
            targetProductionBuildingToDelivery = null;
            targetRocketLauncher = null;
        }

        /// <summary>
        /// Controls drone landing to droneStation
        /// </summary>
        private void CheckForDroneHeight()
        {
            if (targetPosAtDroneStation != Vector3.zero &&
                (Math.Abs(targetPosAtDroneStation.x - _transform.position.x) <= 5 &&
                 Math.Abs(targetPosAtDroneStation.z - _transform.position.z) <= 5))
            {
                model.transform.position = Vector3.MoveTowards(model.transform.position, targetPosAtDroneStation,
                    landingSpeed * Time.deltaTime);
                if (model.transform.position == targetPosAtDroneStation)
                {
                    //landed
                    targetPosAtDroneStation = Vector3.zero;
                    SetEngineParticleState(false);
                    forceMoveToDroneStation = false;
                }
            }
            else if (targetPosAtDroneStation == Vector3.zero && model.transform.localPosition != _defaultModelPosition)
            {
                model.transform.localPosition = Vector3.MoveTowards(model.transform.localPosition,
                    _defaultModelPosition,
                    landingSpeed * Time.deltaTime); //going for defaultHeight
            }
        }

        private void FixedUpdate()
        {
            CheckForLogicMistakes();
            CheckForDroneHeight();
            if (forceMoveToDroneStation)
            {
                SendDroneToDroneStation(true);
                return;
            }

            if (_navMeshAgent.velocity == Vector3.zero)
            {
                TasksIfDroneHaveResources();
            }

            if (currentTarget == null || (targetProductionBuildingToDelivery == null && targetRocketLauncher == null))
            {
                FindTaskForThisDrone();
            }

            TasksIfDroneNearTargets();
        }

        private void CheckForLogicMistakes()
        {
            StorageInfo currentTargetStorageInfo = null;
            if(currentTarget != null) currentTargetStorageInfo = currentTarget.GetComponentInParent<StorageInfo>();
            
            //if no resources but have target to deliver
            if (_droneStorageInfo.resourceUnits.Count == 0)
            {
                if (currentTarget == null && (targetProductionBuildingToDelivery != null || targetRocketLauncher == null))
                {
                    targetProductionBuildingToDelivery = null;
                    targetRocketLauncher = null;
                }
            }
            if (currentTargetStorageInfo != null)
            {
                if(currentTargetStorageInfo.giveResources == false || currentTargetStorageInfo.resourceUnits.Count == 0)
                {
                    currentTarget = null;
                    targetProductionBuildingToDelivery = null;
                    targetRocketLauncher = null;
                }
            }

            if (targetProductionBuildingToDelivery != null &&
                targetProductionBuildingToDelivery.storageInfoIn.GetPointToMove() == null)
            {
                targetProductionBuildingToDelivery = null;
            }
            if (targetRocketLauncher != null &&
                targetRocketLauncher.storageInfoIn.GetPointToMove() == null)
            {
                targetRocketLauncher = null;
            }
        }
        private void TasksIfDroneNearTargets()
        {
            //if near load storage
            if (currentTarget != null && Math.Abs(_transform.position.x - currentTarget.position.x) <= 1 &&
                Math.Abs(_transform.position.z - currentTarget.position.z) <= 1)
            {
                GetResourcesToDrone();
            }
            else
            {
                //if near delivery targets
                if (targetProductionBuildingToDelivery == null && targetRocketLauncher == null) return;
                var pos = new Vector3(-999, -999, -999);
                if (targetProductionBuildingToDelivery != null)
                    pos = targetProductionBuildingToDelivery.storageInfoIn.transform.position;
                if (targetRocketLauncher != null)
                    pos = targetRocketLauncher.storageInfoIn.transform.position;
                if (Math.Abs(_transform.position.x - pos.x) <= 1 &&
                    Math.Abs(_transform.position.z - pos.z) <= 1)
                {
                    GiveResourcesToStorage();
                }
            }
        }

        private void TasksIfDroneHaveResources()
        {
            if (_droneStorageInfo.resourceUnits.Count > 0)
            {
                if (targetProductionBuildingToDelivery != null)
                    MoveDroneTo(targetProductionBuildingToDelivery.storageInfoIn.transform.position);
                if (targetRocketLauncher != null)
                    MoveDroneTo(targetRocketLauncher.storageInfoIn.transform.position);
            }
            else if (currentTarget != null) MoveDroneTo(currentTarget.position);
        }

        private bool CheckIsThisResourceNeededForProduction(ObjectRemoverManager.ObjectType needToCheck,
            ProductionBuildings productionBuildings)
        {
            if (productionBuildings.storageInfoIn != null && (productionBuildings.storageInfoIn.GetPointToMove() == null
                || !productionBuildings.storageInfoIn.IsNeedThisResource(needToCheck))) return false;
            foreach (var type in productionBuildings.consumableResourceType)
            {
                if (needToCheck == type) return true;
            }

            return false;
        }

        private void SetEngineParticleState(bool isEnabled)
        {
            foreach (var particle in _particleSystems)
            {
                if (isEnabled) particle.Play();
                else particle.Stop();
            }
        }

        private void Awake()
        {
            _particleSystems.AddRange(GetComponentsInChildren<ParticleSystem>());
            _defaultModelPosition = model.transform.localPosition;
            _transform = transform;
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _droneStorageInfo = GetComponentInChildren<StorageInfo>();
        }
    }
}