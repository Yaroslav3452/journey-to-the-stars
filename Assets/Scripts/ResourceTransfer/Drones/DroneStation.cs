using System.Collections.Generic;
using LevelManagerScripts;
using ResourcesAndProduction.ResourceStorages;
using ResourcesAndProduction.ResourcesTypes;
using UnityEngine;

namespace ResourceTransfer.Drones
{

    public class DroneStation : MonoBehaviour
    {
        public LevelManager planetParentLevelManager;
        [SerializeField] private int droneMaxCount = 0;
        [SerializeField] private List<Drone> drones = new List<Drone>();
        [SerializeField] private List<Transform> dronesSpots = new List<Transform>();
        [SerializeField] private Transform consumerPoint;
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private int dronePrice;
        private StorageInfo _storageInfo;

        public int ReturnCountOfDrones()
        {
            return drones.Count;
        }
        public void SpawnDronesAtStart(int count)
        {
            for (int i = 0; i < count; i++)
            {
                InstantDroneSpawn();
            }
        }
        public bool IsNeedDroneParts()
        {
            if (droneMaxCount > 0) return true;
            return false;
        }
        private Transform GetPointForSpawnNewDrone()
        {
            foreach (var spawnPointForDrone in dronesSpots)
            {
                if (spawnPointForDrone.childCount == 0)
                {
                    return spawnPointForDrone;
                }
            }

            return null;
        }
        public Transform GetPointForDock(Drone drone)
        {
            var idx = drones.IndexOf(drone);
            return dronesSpots[idx];
        }

        private void InstantDroneSpawn()
        {
            if (droneMaxCount <= 0)
            {
                _storageInfo.getResources = false;
                _storageInfo.giveResources = true;
            }
            var droneGameObject = Instantiate(PrefabsStorage.Current.drone, spawnPoint.position, Quaternion.identity,
                transform);
            var drone = droneGameObject.GetComponent<Drone>();
            drones.Add(drone);
            droneMaxCount--;
            drone.parentDroneStation = this;
            drone.SendDroneToDroneStation(true);
        }

        private void SpawnDrone()
        {
            if(_storageInfo.resourceUnits.Count < dronePrice) return;
            if (droneMaxCount <= 0)
            {
                _storageInfo.getResources = false;
                _storageInfo.giveResources = true;
            }

            for (int i = 0; i < dronePrice; i++)
            {
                var resource = _storageInfo.resourceUnits[_storageInfo.resourceUnits.Count - 1];
                var script = resource.GetComponent<ResourceObject>();
                _storageInfo.resourceUnits.Remove(resource);
                script.isNeedToRemoveAfterMoving = true;
                script.waypoints.Add(consumerPoint);
            }

            // var target = GetPointForSpawnNewDrone();
            // if (target == null) return;
            var droneGameObject = Instantiate(PrefabsStorage.Current.drone, spawnPoint.position,  Quaternion.identity, transform);
            var drone = droneGameObject.GetComponent<Drone>();
            drones.Add(drone);
            droneMaxCount--;
            drone.parentDroneStation = this;
        }

        private void FixedUpdate()
        {
            if (_storageInfo.resourceUnits.Count >= dronePrice)
            {
                SpawnDrone();
            }
        }

        private void Start()
        {
            _storageInfo = GetComponentInChildren<StorageInfo>();
            planetParentLevelManager = GetComponentInParent<LevelManager>();
            droneMaxCount = dronesSpots.Count;
        }
    }
}