﻿using SimpleKeplerOrbits;
using UnityEngine;

namespace solar_system
{
    public class PlanetRotating : MonoBehaviour
    {
        [SerializeField] private float fullRotateTime;
        [SerializeField] private float timeScale;
        private Transform _transform;
        private float _rotationInDegrees;
        private KeplerOrbitMover _keplerOrbitMover;

        private void Start()
        {
            fullRotateTime = fullRotateTime * 24 * 60 * 60;
            _transform = gameObject.transform;
            _keplerOrbitMover = GetComponentInParent<KeplerOrbitMover>();
            timeScale = _keplerOrbitMover.TimeScale;
            _rotationInDegrees = 360 / fullRotateTime * Time.fixedDeltaTime;
        }

        private void FixedUpdate()
        { 
            timeScale = _keplerOrbitMover.TimeScale;
         _transform.Rotate(0f,0f,_rotationInDegrees * timeScale);   
        }
    }
}
