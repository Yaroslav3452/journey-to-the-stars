﻿using UnityEngine;

namespace solar_system
{
    public class PlanetConstraint : MonoBehaviour
    {
        public Transform targetPlanet;
        private Transform _playerTransform;

        private void Start()
        {
            _playerTransform = gameObject.transform;
        }

        private void FixedUpdate()
        {
            var rotation = Quaternion.FromToRotation(-_playerTransform.up, targetPlanet.position - _playerTransform.position);
            _playerTransform.rotation = rotation * _playerTransform.rotation;
        }
    }
}
