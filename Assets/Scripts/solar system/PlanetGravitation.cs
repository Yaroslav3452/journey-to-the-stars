﻿using System.Collections.Generic;
using UnityEngine;

namespace solar_system
{
    public class PlanetGravitation : MonoBehaviour
    {
        private HashSet<Rigidbody> _affectedBodies = new HashSet<Rigidbody>();
        private Rigidbody _planetRigidbody;
        private Transform _planetTransform;

        private void Start()
        {
            _planetRigidbody = GetComponent<Rigidbody>();
            _planetTransform = gameObject.transform;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody != null)
            {
                _affectedBodies.Add(other.attachedRigidbody);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.attachedRigidbody != null)
            {
                _affectedBodies.Remove(other.attachedRigidbody);
            }
        }

        private void FixedUpdate()
        {
            foreach (var body in _affectedBodies)
            {
                var position = _planetTransform.position;
                var position1 = body.position;
                Vector3 forceDirection = (position - position1).normalized;
                float distanceSqr = (position - position1).sqrMagnitude;
                float strength = 10 * _planetRigidbody.mass * body.mass / distanceSqr;
                body.AddForce(forceDirection * strength);
            }
        }
    }
}
