using UnityEngine;

namespace Ui_and_other.UI
{
    public class ButtonsAppearFromPoint : MonoBehaviour
    {
        [SerializeField] private Transform targetPointForDisappear;
        [SerializeField] private float speed;
        private Vector3 _targetPos = new Vector3(-1, -1, -1);
        private Vector3 _defaultPosition;
        private bool _isMoving;

        public void SmoothAppear()
        {
            _targetPos = _defaultPosition;
            _isMoving = true;
        }

        public void SmoothDisappear()
        {
            _targetPos = targetPointForDisappear.position;
            _isMoving = true;
        }

        private void OnEnable()
        {
            SmoothAppear();
        }

        private void OnDisable()
        {
            transform.position = targetPointForDisappear.position;
        }

        private void Update()
        {
            if (_isMoving)
            {
                transform.position = Vector3.Lerp(transform.position, _targetPos, Time.deltaTime * speed);
                if (transform.position == _defaultPosition)
                {
                    _targetPos = new Vector3(-1, -1, -1);
                    _isMoving = false;
                }
            }
        }

        /// <summary>
        /// switch parent panel off at start
        /// </summary>
        private void Awake()
        {
            var transform1 = transform;
            _defaultPosition = transform1.position;
            transform1.position = targetPointForDisappear.position;

        }
    }
}