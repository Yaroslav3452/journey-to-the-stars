using System;
using UnityEngine;
using UnityEngine.UI;

namespace Ui_and_other.UI
{
    public class TransparencyChanger : MonoBehaviour
    {
        [SerializeField] private float speed = 3;
        private Image _image;
        private float _targetAlpha;

        public void SetTargetTransparency(float target)
        {
            _targetAlpha = target;
        }

        private void Update()
        {
            if(Math.Abs(_image.color.a - _targetAlpha) > 0)
            {
                _image.color = Color.Lerp(_image.color,
                    new Color( _image.color.r, _image.color.g, _image.color.b, _targetAlpha),
                    speed * Time.deltaTime);
            }
        }

        private void Awake()
        {
            _image = GetComponent<Image>();
            _targetAlpha = _image.color.a;
        }
    }
}