﻿using System;
using Astronaut;
using UnityEngine;

namespace Ui_and_other
{
    public class ToPlayerRotator : MonoBehaviour
    {
        public Transform cameraTransform;
        private Transform _transform;
        private Transform _playerTransform;

        private void Update()
        { 
            //_transform.LookAt(_playerTransform);
            if(cameraTransform != null)_transform.LookAt(cameraTransform);
        }

        void Start()
        {
            _transform = transform;
            _playerTransform = PlayerResourcesStorage.Current.gameObject.transform;
            if (Camera.main != null) cameraTransform = Camera.main.transform;
        }
    }
}
