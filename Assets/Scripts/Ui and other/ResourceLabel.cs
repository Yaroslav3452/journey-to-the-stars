﻿using ResourcesAndProduction;
using TMPro;
using UnityEngine;

namespace Ui_and_other
{
    public class ResourceLabel : MonoBehaviour
    {
        public TextMeshPro textPanel;
        public ConstructionManager parentConstructionManager;
        [SerializeField] private Vector3 maxSize = new Vector3(1.5f, 1.5f, 1.5f);
        [SerializeField] private Vector3 minimumSize = Vector3.one;
        [SerializeField] bool isRemoving;
        private Transform _transform;
        private Vector3 _targetScale = Vector3.zero;
        private bool _isParentConstructionManagerNotNull;

        /// <summary>
        /// Sets text info of parent object. For example, rocket launcher export
        /// </summary>

        private void SetText()
        {
            textPanel.SetText("");
            if(_isParentConstructionManagerNotNull)
            {
                if (parentConstructionManager.oilNeeded > 0)
                    textPanel.text += "oil: " + parentConstructionManager.oilNeeded + "\n";
                if (parentConstructionManager.fuelNeeded > 0)
                    textPanel.text += "fuel: " + parentConstructionManager.fuelNeeded + "\n";
                if (parentConstructionManager.simpleCircuitsNeeded > 0)
                    textPanel.text += "Simple Circuits: " + parentConstructionManager.simpleCircuitsNeeded + "\n";
                if (parentConstructionManager.dronePartsNeeded > 0)
                    textPanel.text += "Drone Parts: " + parentConstructionManager.dronePartsNeeded + "\n";
                if (parentConstructionManager.buildingPartsNeeded > 0)
                    textPanel.text += "Building Parts: " + parentConstructionManager.buildingPartsNeeded + "\n";
                if (parentConstructionManager.superTechnologicalPartsNeeded > 0)
                    textPanel.text += "HighTech Parts: " + parentConstructionManager.superTechnologicalPartsNeeded + "\n";
                if (parentConstructionManager.titaniumOreNeeded > 0)
                    textPanel.text += "Titanium Ore: " + parentConstructionManager.titaniumOreNeeded + "\n";
                if (parentConstructionManager.titaniumIngotNeeded > 0)
                    textPanel.text += "Titanium Ingot: " + parentConstructionManager.titaniumIngotNeeded + "\n";
                if (parentConstructionManager.concreteNeeded > 0)
                    textPanel.text += "Concrete: " + parentConstructionManager.concreteNeeded + "\n";
                if (parentConstructionManager.martianRegoliteNeeded > 0)
                    textPanel.text += "Martian Regolite: " + parentConstructionManager.martianRegoliteNeeded + "\n";
                if (parentConstructionManager.superMetalNeeded > 0)
                    textPanel.text += "Super Metal: " + parentConstructionManager.superMetalNeeded + "\n";
                if (parentConstructionManager.alienArtifactPartsNeeded > 0)
                    textPanel.text += "Artifact Parts: " + parentConstructionManager.alienArtifactPartsNeeded + "\n";
                if (parentConstructionManager.aliveMetalNeeded > 0)
                    textPanel.text += "Alive metal: " + parentConstructionManager.aliveMetalNeeded + "\n";
                if (parentConstructionManager.alienArtifactsNeeded > 0)
                    textPanel.text += "Artifacts: " + parentConstructionManager.alienArtifactsNeeded + "\n";
                if (parentConstructionManager.alienCircuitsNeeded > 0)
                    textPanel.text += "Alien Circuits: " + parentConstructionManager.alienCircuitsNeeded + "\n";
                if(textPanel.text.Length >= 2) textPanel.text.Remove(textPanel.text.Length - 1).Remove(textPanel.text.Length - 2);
                //removes last "\n"
            }
        }

        public void SetScaleToMaximum()
        {
            _targetScale = maxSize;
        }
        public void SetScaleToMinimum()
        {
            _targetScale = minimumSize;
        }

        public void SmoothDisappear()
        {
            isRemoving = true;
        }

        private void Update()
        { 
            SetText();
            if (isRemoving)
            {
                _transform.localScale = Vector3.Lerp(_transform.localScale, Vector3.zero, 2f * Time.deltaTime);
                if(_transform.localScale.magnitude <= new Vector3(.1f,.1f,.1f).magnitude) gameObject.SetActive(false);
            }

            if (_targetScale != Vector3.zero && _transform.localScale != _targetScale)
            {
                _transform.localScale = Vector3.Lerp(_transform.localScale, _targetScale, 2f * Time.deltaTime);
            }
        }

        private void Start()
        {
            textPanel = GetComponentInChildren<TextMeshPro>();
            parentConstructionManager = GetComponentInParent<ConstructionManager>();
            _isParentConstructionManagerNotNull = parentConstructionManager != null;
            _transform = transform;
        }
    }
}
