using UnityEngine;

namespace Ui_and_other
{
    public class UISmoothAppearing : MonoBehaviour
    {
        [SerializeField] private GameObject parentObject;
        [SerializeField] private Transform targetPointForDissapear;
        private Vector3 _targetSize = new Vector3(-1, -1, -1);
        private Vector3 _targetPos = new Vector3(-1, -1, -1);
        private Vector3 _defaultSize;
        private Vector3 _defaultPosition;

        public void SmoothOpenMenu()
        {
            parentObject.SetActive(true);
            _targetSize = _defaultSize;
            _targetPos = _defaultPosition;
        }

        public void SmoothCloseMenu()
        {
            _targetSize = Vector3.zero;
            _targetPos = targetPointForDissapear.position;
        }

        private void Update()
        {
            if (parentObject.activeSelf)
            {
                if (_targetSize != new Vector3(-1, -1, -1))
                {
                    var parentObjectTransform = parentObject.transform;
                    parentObjectTransform.localScale = Vector3.Lerp(parentObjectTransform.localScale, _targetSize, .1f);
                    parentObjectTransform.position = Vector3.Lerp(parentObject.transform.position, _targetPos, .1f);
                    if (parentObjectTransform.localScale == _targetSize)
                    {
                        if (_targetSize == Vector3.zero) parentObject.SetActive(false);
                        _targetSize = new Vector3(-1, -1, -1);
                        _targetPos = new Vector3(-1, -1, -1);
                    }
                }
            }
        }
/// <summary>
/// switch parent panel off at start
/// </summary>
        private void Awake()
        {
            _defaultSize = parentObject.transform.localScale;
            _defaultPosition = parentObject.transform.position;
            _targetSize = new Vector3(-1, -1, -1);
            parentObject.transform.localScale = Vector3.zero; //autoClose
            parentObject.transform.position = Vector3.zero; //autoClose
            parentObject.SetActive(false);
        }
    }
}