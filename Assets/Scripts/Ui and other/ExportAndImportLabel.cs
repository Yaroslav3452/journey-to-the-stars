﻿using ResourcesAndProduction.ProductionBuildings;
using ResourcesAndProduction.Rocket;
using TMPro;
using UnityEngine;

namespace Ui_and_other
{
    public class ExportAndImportLabel : MonoBehaviour
    {
        public TextMeshPro textPanel;
        public RocketLauncher parentRocketLauncher;
        public ProductionBuildings productionBuilding;

        /// <summary>
        /// Sets text info of parent object. For example, rocket launcher export
        /// </summary>

        private void SetText()
        {
            if (parentRocketLauncher != null) //set info for rocket launcher
            {
                textPanel.SetText("Import:\n");
                foreach (var type in parentRocketLauncher.imported)
                {
                    textPanel.text += type.ToString() + "  ";
                }

                textPanel.text += "\n" + "Export: \n";
                foreach (var type in parentRocketLauncher.exported)
                {
                    textPanel.text += type.ToString() + "  ";
                }
            }

            if (productionBuilding != null)
            {
                textPanel.SetText("Consume:\n");
                foreach (var type in productionBuilding.consumableResourceType)
                {
                    textPanel.text += type.ToString() + "  ";
                }
                textPanel.text += "\n" + "Product: \n";
                textPanel.text += productionBuilding.Product.ToString() + "  ";
            }
        }

        private void Start()
        {
            textPanel = GetComponentInChildren<TextMeshPro>();
            parentRocketLauncher = GetComponentInParent<RocketLauncher>();
            productionBuilding = GetComponentInParent<ProductionBuildings>();
            SetText();
        }
    }
}