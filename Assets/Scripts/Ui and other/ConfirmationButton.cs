using System;
using UnityEngine;

namespace Ui_and_other
{

    public class ConfirmationButton : MonoBehaviour
    {
        public bool isConfirmed = false;
        private UISmoothAppearing _uiSmoothAppearing;

        public void OpenMenu()
        {
            _uiSmoothAppearing.SmoothOpenMenu();
        }

        public void CloseMenu()
        {
            _uiSmoothAppearing.SmoothCloseMenu();
        }

        public void SetConfirmation()
        {
            isConfirmed = true;
            CloseMenu();
        }

        private void Awake()
        {
            _uiSmoothAppearing = GetComponent<UISmoothAppearing>();
            CloseMenu();
        }
    }
}