using System.Collections.Generic;
using LevelManagerScripts;
using PlayerTransferBetweenPlanets;
using ResourceTransfer;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Ui_and_other
{
    public class PlanetChoosePopUpWindow : MonoBehaviour

    {
        public static PlanetChoosePopUpWindow Current;
        [SerializeField] private GameObject earthButton;
        [SerializeField] private GameObject moonButton;
        [SerializeField] private GameObject marsButton;
        [SerializeField] private GameObject alienButton;
        private PointerEventData _pointerEventData;
        private List<RaycastResult> _castResults = new List<RaycastResult>();
        private UISmoothAppearing _uiSmoothAppearing;
        public enum PlanetsEnum
        {
            Earth = 0,
            Moon,
            Mars,
            Alien
        }

        public void SetTargetPlanet(int planetType)
        {
            var button = ReturnNeededButton((PlanetsEnum)planetType);
            var buttonComponent = button.GetComponentInChildren<Button>();
            var text = button.GetComponentInChildren<TextMeshProUGUI>();
            SetDefaultColorForAllButtonsText();
            text.color = Color.red;
            LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.targetDestination =
                ResourceTransferSystem.Current.ReturnNeededRocketLauncherByType((PlanetsEnum)planetType);
            if (LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.targetDestination != null) 
                PlayerTransferSystem.Current.SetInteractableForRelocateButton(true);
        }

        public void OpenMenu()
        {
            CheckForButtons();
            if (LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.targetDestination == null)
            {
                PlayerTransferSystem.Current.SetInteractableForRelocateButton(false);
            } else  PlayerTransferSystem.Current.SetInteractableForRelocateButton(true);
            _uiSmoothAppearing.SmoothOpenMenu();
        }

        public void CloseMenu()
        {
            _uiSmoothAppearing.SmoothCloseMenu();
        }
        
/// <summary>
/// disable button of current rocket launcher
/// </summary>
        private void CheckForButtons()
        {
            earthButton.SetActive(true);
            moonButton.SetActive(true);
            marsButton.SetActive(true);
            alienButton.SetActive(true);
            switch (LevelsSwitcherSystem.Current.currentLevelManager.rocketLauncher.ReturnPlanetType())
            {
                case PlanetsEnum.Earth:
                    earthButton.SetActive(false);
                    break;
                case PlanetsEnum.Moon:
                    moonButton.SetActive(false);
                    break;
                case PlanetsEnum.Mars:
                    marsButton.SetActive(false);
                    break;
                case PlanetsEnum.Alien:
                    alienButton.SetActive(false);
                    break;
            }
        }
        public bool IsPointerOverPopUpWindow(Vector3 clickPosition)
        {
            _pointerEventData.position = clickPosition;
            EventSystem.current.RaycastAll(_pointerEventData, _castResults);
            foreach (var t in _castResults)
            {
                if (t.gameObject.layer == 13)
                {
                    return true;
                }
            }

            return false;
        }

        private void SetDefaultColorForAllButtonsText()
        {
            earthButton.GetComponentInChildren<TextMeshProUGUI>().color = Color.white; 
            moonButton.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            marsButton.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            alienButton.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;


        }

        private GameObject ReturnNeededButton(PlanetsEnum planetType)
        {
            switch (planetType)
            {
                case PlanetsEnum.Earth:
                    return earthButton;
                case PlanetsEnum.Moon:
                    return moonButton;
                case PlanetsEnum.Mars:
                    return marsButton;
                case PlanetsEnum.Alien:
                    return alienButton;
            }
            return null;
        }
        private void Awake()
        {
            Current = this;
            _uiSmoothAppearing = GetComponent<UISmoothAppearing>();
            _pointerEventData = new PointerEventData(EventSystem.current);
        }
    }
}