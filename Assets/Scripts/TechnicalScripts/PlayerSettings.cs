﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TechnicalScripts
{
    public class PlayerSettings : MonoBehaviour
    {
        public static PlayerSettings Current;
        public int vibration;
        public int qualitySettings;
        public int volume;
        public int difficulty;
        [SerializeField] private Slider volumeSlider;
        private void Awake()
        {
            Current = this;
            LoadGameSettings();
        }

        private void SetUIValuesOnStart()
        {
            if (volumeSlider != null) volumeSlider.value = volume;
            if (CanvasesManager.Current != null)
            {
                CanvasesManager.Current.SetVibration();
            }
            if (SoundManager.Current != null) SoundManager.Current.SetMasterVolume();
        }

        public void CheckSliderValues()
        {
            volume = (int) volumeSlider.value;
            SoundManager.Current.SetMasterVolume();
        }

        public void SaveGameSettings()
        {
            PlayerPrefs.SetInt("SavedVibration", vibration);
            PlayerPrefs.SetInt("SavedQuality", qualitySettings);
            PlayerPrefs.SetInt("SavedVolume", volume);
            PlayerPrefs.SetInt("Difficulty", difficulty);
        }
        public void ChangeQuality(bool isStartSetter)
        {
            if(!isStartSetter) qualitySettings = qualitySettings == 1 ? 0 : 1;
            QualitySettings.SetQualityLevel(qualitySettings);
            CanvasesManager.Current.SetQuality();
        }
        public void ChangeVibration()
        {
            vibration = vibration == 1 ? 0 : 1;
            CanvasesManager.Current.SetVibration();
        }
        
        private void LoadGameSettings()
        {
            if (PlayerPrefs.HasKey("SavedVibration")) vibration = PlayerPrefs.GetInt("SavedVibration");
            if (PlayerPrefs.HasKey("SavedQuality"))
            {
                qualitySettings = PlayerPrefs.GetInt("SavedQuality");
                ChangeQuality(true);
            }
            if (PlayerPrefs.HasKey("Difficulty")) difficulty = PlayerPrefs.GetInt("Difficulty");
            if (PlayerPrefs.HasKey("SavedVolume")) volume = PlayerPrefs.GetInt("SavedVolume");
            SetUIValuesOnStart();
        }
    }
}
