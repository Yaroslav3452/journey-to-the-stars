using Astronaut;
using CI.QuickSave;
using LevelManagerScripts;
using ResourcesAndProduction;
using ResourcesAndProduction.ResourceStorages;
using ResourceTransfer.Drones;
using UnityEngine;
using UnityEngine.Windows;
// ReSharper disable RedundantDefaultMemberInitializer

namespace TechnicalScripts.SaveManager
{
    public class SaveManager : MonoBehaviour
    {
        public static SaveManager Current;
        [SerializeField] private bool clearSaves = false;

        /// <summary>
        /// Save all at disk at path Application.persistentDataPath
        /// </summary>
        public void SaveAll()
        {
            if (Directory.Exists((QuickSaveGlobalSettings.StorageLocation)))
            {
                SavePlayer();
                SaveStorages();
                SaveConstruction();
                SaveTechnicalData();
                SaveDroneStations();
                SaveLevelManagers();
                if (clearSaves) ClearAllSaves();
            }
        }
        /// <summary>
        /// saves position, resources of player
        /// </summary>
        private void SavePlayer()
        {
            var quickSaveWriter = QuickSaveWriter.Create("Player", new QuickSaveSettings()
            {
                SecurityMode = SecurityMode.None,
                CompressionMode = CompressionMode.None
            });
            quickSaveWriter.Write("name", "Player");
            quickSaveWriter.Write("Position", PlayerMovement.Current.gameObject.transform.position);
            quickSaveWriter.Write("oil", PlayerResourcesStorage.Current.Oils.Count);
            quickSaveWriter.Write("fuel", PlayerResourcesStorage.Current.Fuels.Count);
            quickSaveWriter.Write("droneParts", PlayerResourcesStorage.Current.DroneParts.Count);
            quickSaveWriter.Write("artifacts", PlayerResourcesStorage.Current.Artifacts.Count);
            quickSaveWriter.Write("concretes", PlayerResourcesStorage.Current.Concretes.Count);
            quickSaveWriter.Write("droneParts", PlayerResourcesStorage.Current.DroneParts.Count);
            quickSaveWriter.Write("circuits", PlayerResourcesStorage.Current.SimpleCircuits.Count);
            quickSaveWriter.Write("alienCircuits", PlayerResourcesStorage.Current.AlienCircuits.Count);
            quickSaveWriter.Write("buildingParts", PlayerResourcesStorage.Current.BuildingsParts.Count);
            quickSaveWriter.Write("superTechnologicalParts", PlayerResourcesStorage.Current.SuperTechnologicalParts.Count);
            quickSaveWriter.Write("titaniumOre", PlayerResourcesStorage.Current.TitaniumOres.Count);
            quickSaveWriter.Write("titaniumIngots", PlayerResourcesStorage.Current.TitaniumIngots.Count);
            quickSaveWriter.Write("martianRegolite", PlayerResourcesStorage.Current.MartianRegolites.Count);
            quickSaveWriter.Write("superMetals", PlayerResourcesStorage.Current.SuperMetals.Count);
            quickSaveWriter.Write("artifactParts", PlayerResourcesStorage.Current.ArtifactParts.Count);
            quickSaveWriter.Write("aliveMetal", PlayerResourcesStorage.Current.AliveMetals.Count);
            quickSaveWriter.Commit();
        }

        private void LoadPlayer()
        {
            var quickSaveReader = QuickSaveReader.Create("Player");
            quickSaveReader.GetAllKeys();
            quickSaveReader.TryRead<Vector3>("Position", out var pos);
            quickSaveReader.TryRead<int>("oil", out var countOil);
            quickSaveReader.TryRead<int>("fuel", out var fuels);
            quickSaveReader.TryRead<int>("artifacts",out var artifacts);
            quickSaveReader.TryRead<int>("concretes", out var concretes);
            quickSaveReader.TryRead<int>("droneParts", out var droneParts);
            quickSaveReader.TryRead<int>("circuits", out var circuits);
            quickSaveReader.TryRead<int>("alienCircuits" , out var alienCircuits);
            quickSaveReader.TryRead<int>("buildingParts",out var buildingParts);
            quickSaveReader.TryRead<int>("superTechnologicalParts", out var superTechnologicalParts);
            quickSaveReader.TryRead<int>("titaniumOre",out var titaniumOre);
            quickSaveReader.TryRead<int>("titaniumIngots", out var titaniumIngot);
            quickSaveReader.TryRead<int>("martianRegolite", out var martianRegolite);
            quickSaveReader.TryRead<int>("superMetals", out var superMetals);
            quickSaveReader.TryRead<int>("artifactParts", out var artifactParts);
            quickSaveReader.TryRead<int>("aliveMetal", out var aliveMetal);
            PlayerMovement.Current.WarpPlayerToPoint(pos);
            PlayerResourcesStorage.Current.SpawnResourcesAtPlayerStorage(countOil, fuels, droneParts, buildingParts, circuits,
                superTechnologicalParts,  titaniumOre,titaniumIngot,concretes , martianRegolite,
                superMetals, artifactParts, aliveMetal, artifacts, alienCircuits);
        }
        
        private void SaveTechnicalData()
        {
            var quickSaveWriter = QuickSaveWriter.Create("Technical", new QuickSaveSettings()
            {
                SecurityMode = SecurityMode.None,
                CompressionMode = CompressionMode.None
            });
            quickSaveWriter.Write("name", "TechnicalData");
            quickSaveWriter.Write("LevelManagerIdx", LevelsSwitcherSystem.Current.ReturnCurrentLevelIdx());
            quickSaveWriter.Commit();
        }

        private void LoadCurrentLevelManager()
        {
            var quickSaveReader = QuickSaveReader.Create("Technical");
            quickSaveReader.GetAllKeys();
            quickSaveReader.TryRead<int>("LevelManagerIdx", out var idx);
            LevelsSwitcherSystem.Current.SetCurrentLevelManager(idx);
        }

        private void SaveLevelManagers()
        {
            var levelManagers = GameObject.FindObjectsOfType<LevelManager>();
            foreach (var level in levelManagers)
            {
                var quickSaveWriter = QuickSaveWriter.Create("levelManager - " + level.name, new QuickSaveSettings()
                {
                    SecurityMode = SecurityMode.None,
                    CompressionMode = CompressionMode.None
                });
                quickSaveWriter.Write("name", level.name);
                quickSaveWriter.Write("LevelManagerStage", level.levelStage);
                quickSaveWriter.Commit();
            }
        }
        private void LoadLevelManagers()
        {
            var levelManagers = GameObject.FindObjectsOfType<LevelManager>();
            foreach (var level in levelManagers)
            {
                var quickSaveReader = QuickSaveReader.Create("levelManager - " + level.name);
                quickSaveReader.TryRead<int>("LevelManagerStage", out var levelStage);
                level.SetLevelStage(levelStage);
            }
        }
        private void SaveStorages()
        {
            var storages = GameObject.FindObjectsOfType<StorageInfo>();
            foreach (var storage in storages)
            {
                var parent = storage.transform.parent;
                var quickSaveWriter = QuickSaveWriter.Create( storage.name + "-" + parent.name, new QuickSaveSettings()
                {
                    SecurityMode = SecurityMode.None,
                    CompressionMode = CompressionMode.None
                });
                quickSaveWriter.Write("name",
                    storage.name + "-" + parent.name);
                quickSaveWriter.Write("oil", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Oil));
                quickSaveWriter.Write("fuel", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Fuel));
                quickSaveWriter.Write("droneParts", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.DroneParts));
                quickSaveWriter.Write("artifacts",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Artifacts));
                quickSaveWriter.Write("concretes",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Concrete));
                quickSaveWriter.Write("droneParts", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.DroneParts));
                quickSaveWriter.Write("circuits", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Circuits));
                quickSaveWriter.Write("alienCircuits", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.AlienCircuits));
                quickSaveWriter.Write("buildingParts",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.BuildingParts) );
                quickSaveWriter.Write("superTechnologicalParts",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.HighTechParts) );
                quickSaveWriter.Write("titaniumOre", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.TitaniumOre));
                quickSaveWriter.Write("titaniumIngots", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.Titanium));
                quickSaveWriter.Write("martianRegolite",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.MartianRegolite) );
                quickSaveWriter.Write("superMetals",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.SuperMetal) );
                quickSaveWriter.Write("artifactParts",storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.ArtifactParts) );
                quickSaveWriter.Write("aliveMetal", storage.ReturnCountOfThisResource(ObjectRemoverManager.ObjectType.AliveMetal));
                quickSaveWriter.Commit();
            }
        }

        private void LoadStorages()
        {
            var storages = GameObject.FindObjectsOfType<StorageInfo>();
            foreach (var storage in storages)
            {
                var quickSaveReader = QuickSaveReader.Create(storage.name + "-" + storage.transform.parent.name);
                quickSaveReader.TryRead<int>("oil", out var countOil);
                quickSaveReader.TryRead<int>("fuel", out var fuels);
                quickSaveReader.TryRead<int>("artifacts",out var artifacts);
                quickSaveReader.TryRead<int>("concretes", out var concretes);
                quickSaveReader.TryRead<int>("droneParts", out var droneParts);
                quickSaveReader.TryRead<int>("circuits", out var circuits);
                quickSaveReader.TryRead<int>("alienCircuits" , out var alienCircuits);
                quickSaveReader.TryRead<int>("buildingParts",out var buildingParts);
                quickSaveReader.TryRead<int>("superTechnologicalParts", out var superTechnologicalParts);
                quickSaveReader.TryRead<int>("titaniumOre",out var titaniumOre);
                quickSaveReader.TryRead<int>("titaniumIngots", out var titaniumIngot);
                quickSaveReader.TryRead<int>("martianRegolite", out var martianRegolite);
                quickSaveReader.TryRead<int>("superMetals", out var superMetals);
                quickSaveReader.TryRead<int>("artifactParts", out var artifactParts);
                quickSaveReader.TryRead<int>("aliveMetal", out var aliveMetal);
                storage.SpawnResourcesAtStorage(countOil, fuels, droneParts, buildingParts, circuits,
                    superTechnologicalParts,  titaniumOre,titaniumIngot,concretes , martianRegolite,
                    superMetals, artifactParts, aliveMetal, artifacts, alienCircuits);
            }
        }

        private void SaveConstruction()
        {
            var constructions = GameObject.FindObjectsOfType<ConstructionManager>();
            foreach (var construction in constructions)
            {
                var parent = construction.transform.parent;
                var quickSaveWriter = QuickSaveWriter.Create(construction.name + "-" + parent.name);
                quickSaveWriter.Write("name",
                    construction.name + "-" + parent.name);
                quickSaveWriter.Write("builded", construction.builded);
                quickSaveWriter.Commit();
            }
        }

        private void LoadConstruction()
        {
            var constructions = GameObject.FindObjectsOfType<ConstructionManager>();
            foreach (var construction in constructions)
            {
                var quickSaveReader = QuickSaveReader.Create(construction.name + "-" + construction.transform.parent.name);
                quickSaveReader.TryRead<bool>("builded", out var builded);
                if (builded) construction.InstantConstruction();
            }
        }

        private void SaveDroneStations()
        {
            var droneStations = GameObject.FindObjectsOfType<DroneStation>();
            foreach (var station in droneStations)
            {
                var parent = station.transform.parent;
                var quickSaveWriter = QuickSaveWriter.Create(station.name + "-" + parent.name);
                quickSaveWriter.Write("name", station.name + "-" + parent.name);
                quickSaveWriter.Write("dronesCount", station.ReturnCountOfDrones());
                quickSaveWriter.Commit();
            }
        }
        private void LoadDroneStations()
        {
            var droneStations = GameObject.FindObjectsOfType<DroneStation>();
            foreach (var station in droneStations)
            {
                var quickSaveReader = QuickSaveReader.Create(station.name + "-" + station.transform.parent.name);
                quickSaveReader.TryRead<int>("dronesCount", out var count);
                station.SpawnDronesAtStart(count);
            }
        }

        /// <summary>
        /// remove saves from disk
        /// </summary>
        private void ClearAllSaves()
        {
            Directory.Delete(QuickSaveGlobalSettings.StorageLocation);
            clearSaves = false;
        }

        private void Awake()
        {
            Current = this;
#if UNITY_EDITOR
            if (!Directory.Exists("Assets/Saves"))
            {
                Directory.CreateDirectory("Assets/Saves");
            }
            QuickSaveGlobalSettings.StorageLocation = "Assets/Saves";
#endif
#if UNITY_ANDROID
             QuickSaveGlobalSettings.StorageLocation = Application.persistentDataPath;
#endif
            if (clearSaves) ClearAllSaves();
            if (Directory.Exists(QuickSaveGlobalSettings.StorageLocation + "/QuickSave"))
            {
                LoadPlayer();
                LoadConstruction();
                LoadStorages();
                LoadCurrentLevelManager();
                LoadDroneStations();
                LoadLevelManagers();
            }
        }
    }
}