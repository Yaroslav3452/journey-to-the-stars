using System;
using System.Threading.Tasks;
using TMPro;
using Ui_and_other.UI;
using UnityEngine;

namespace TechnicalScripts
{

    public class CanvasesManager : MonoBehaviour
    {
        public static CanvasesManager Current;
        public GameObject inGameUI;
        public GameObject settings;
        public GameObject startMenu;
        [SerializeField] private TextMeshProUGUI qualityText;
        [SerializeField] private TextMeshProUGUI vibrationText;
        [SerializeField] private float menuCloseDelay = 1f;

        public bool IsAnyMenuIsOpen()
        {
            if (settings.activeSelf || startMenu.activeSelf) return true;
            return false;
        }
        public void SetQuality()
        {
            if(qualityText == null) return;
            qualityText.text = PlayerSettings.Current.qualitySettings == 1 ? "Quality - High" : "Quality - Low";
        }

        public void PrivacyPolicyOpen()
        {
            Application.OpenURL("https://docs.google.com/document/d/1pEnfCv_90MjmWKeh3u7Rgt2R1A6PaV-x3n4wEOF8VNg/edit#heading=h.otk6qn11qptp");
        }

        public async void StartGame()
        {
            SmoothDisappearingForCanvas(startMenu);
            await Waiter(menuCloseDelay);
            if (startMenu != null) startMenu.SetActive(false);
            if (inGameUI != null) inGameUI.SetActive(true);
        }

        public void OpenSettingWindow()
        {
            SmoothDisappearingForCanvas(startMenu);
            SmoothAppearingForCanvas(settings);
        }

        private async void SmoothDisappearingForCanvas(GameObject canvas)
        {
            var smoothScripts = canvas.GetComponentsInChildren<ButtonsAppearFromPoint>();
            foreach (var script in smoothScripts)
            {
                script.SmoothDisappear();
            }
            var transparencyChangers = canvas.GetComponentsInChildren<TransparencyChanger>();
            foreach (var script in transparencyChangers)
            {
                script.SetTargetTransparency(0);
            }
            await Waiter(menuCloseDelay);
            canvas.SetActive(false);
        }

        private static void SmoothAppearingForCanvas(GameObject canvas)
        {
            canvas.SetActive(true);
            var smoothScripts = canvas.GetComponentsInChildren<ButtonsAppearFromPoint>();
            foreach (var script in smoothScripts)
            {
                script.SmoothAppear();
            }
            var transparencyChangers = canvas.GetComponentsInChildren<TransparencyChanger>();
            foreach (var script in transparencyChangers)
            {
                script.SetTargetTransparency(1);
            }
        }

        public void CloseSettingsMenu()
        {
            SmoothDisappearingForCanvas(settings);
            SmoothAppearingForCanvas(startMenu);
            PlayerSettings.Current.SaveGameSettings();
        }

        public void SetVibration()
        {
            if(vibrationText == null) return;
            vibrationText.text = PlayerSettings.Current.vibration == 1 ? "Vibration - on" : "Vibration - off";
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
#if UNITY_ANDROID
            Application.Quit();
#endif
        }

        private static async Task Waiter(float time)
        {
            await Task.Delay(TimeSpan.FromSeconds(time));
        }

        private void Awake()
        {
            Current = this;
        }
    }
}