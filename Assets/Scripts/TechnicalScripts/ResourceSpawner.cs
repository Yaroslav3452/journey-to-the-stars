﻿using System;
using System.Collections.Generic;
using ResourcesAndProduction.ProductionBuildings;
using UnityEngine;

namespace TechnicalScripts
{
    public class ResourceSpawner : MonoBehaviour
    {
        public static ResourceSpawner Current;
        public List<ProductionBuildings> buildings = new List<ProductionBuildings>();

        private void CheckForBuildings()
        {
            var delta = TimeManager.Current.CalcTimeWhileGameIsOff();
            if(delta.TotalSeconds < 0) Debug.LogError("Wrong deltaTime", this);
            foreach (var factory in buildings)
            {
                var countOfTheoreticalUnits = (int)(delta.TotalSeconds / factory.timePerUnit);
                var neededProductsCount = factory.ReturnCountOfNewProducts();
                int endCount = 0;
                if (countOfTheoreticalUnits > neededProductsCount) endCount = neededProductsCount;
                else endCount = countOfTheoreticalUnits;
                for (int i = 0; i < endCount; i++)
                {
                    factory.ProductResource();
                }
            }
        }
        private void Start()
        {
            Current = this;
            buildings.AddRange(GameObject.FindObjectsOfType<ProductionBuildings>());
            CheckForBuildings();
        }
    }
}