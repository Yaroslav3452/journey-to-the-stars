using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace TechnicalScripts
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Current;
        public AudioSource menuSoundSource;
        public AudioClip clickInMenuSound;
        public List<AudioClip> backgroundMusic = new List<AudioClip>();
        [SerializeField] private AudioMixer audioMixer;
        
        public void ClickInMenu()
        {
            menuSoundSource.Play();
        }
        public void SetMasterVolume()
        {
            audioMixer.SetFloat("Master", -40 + PlayerSettings.Current.volume);
        }

        private void Awake()
        {
            Current = this;
        }
    }
}