using System;
using UnityEditor;
using UnityEngine;

namespace TechnicalScripts
{
    public class WhileSleepManager : MonoBehaviour
    {
        public static WhileSleepManager Current;
        
        private void PrepareGameForSleep()
        {
            TimeManager.Current.SaveTimeOfExit();
        }

        private void OnApplicationQuit()
        {
            PrepareGameForSleep();
            SaveManager.SaveManager.Current.SaveAll();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
        }

        private void Awake()
        {
            Current = this;
        }
    }
}