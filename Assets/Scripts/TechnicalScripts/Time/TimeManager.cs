using System;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;

namespace TechnicalScripts
{
    public class TimeManager : MonoBehaviour
    {
        public static TimeManager Current;
        public bool synced = false;
        public static DateTime UtcStartTime = DateTime.UtcNow;
        public static DateTime UtcNow => UtcStartTime.AddSeconds(Time.realtimeSinceStartup);

        public void SaveTimeOfExit()
        {
            PlayerPrefs.SetString("LastTime", UtcNow.ToString(CultureInfo.InvariantCulture));
        }

        public TimeSpan CalcTimeWhileGameIsOff()
        {
            if (!PlayerPrefs.HasKey("LastTime")) return TimeSpan.Zero;
            var lastTimeString = PlayerPrefs.GetString("LastTime");
            DateTime lastTime =  DateTime.Parse(lastTimeString, CultureInfo.InvariantCulture);
            var delta = UtcNow - lastTime;
            if (delta.TotalSeconds < 0)
            {
                delta = DateTime.UtcNow - lastTime;
                Debug.Log("wrong saved time or bad time sync", this);
            }
            return delta;
        }
        private void Awake()
        {
            Current = this;
            StartSync();
        }

        private async void StartSync()
        {
            await InitializeTime();
            CalcTimeWhileGameIsOff();
        }
        private async Task InitializeTime()
        {
            Debug.Log("startSyncTime");
            await Task.Run(GetTimeFromInternet.InitializeTime);
            Debug.Log("endSyncTime");
            Debug.Log("synced " + synced);
        }
    }
}