using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;

namespace TechnicalScripts
{

    public static class GetTimeFromInternet
    {
        private static DateTime _utcStartTime = DateTime.Now;
        public static DateTime UtcNow => _utcStartTime.AddSeconds(Time.realtimeSinceStartup);

        public static async void InitializeTime()
        {
            await GetUtcTimeAsync().WrapErrors();
        }

        public static void SyncTimeWithTimeManager()
        {
            Debug.Log("Time synced");
            TimeManager.UtcStartTime = _utcStartTime;
            TimeManager.Current.synced = true;
        }
        private static async Task GetUtcTimeAsync()
        {
            try
            {
                var client = new TcpClient();
                await client.ConnectAsync("time.nist.gov", 13);
                var streamReader = new StreamReader(client.GetStream());
                var response = await streamReader.ReadToEndAsync();
                var utcDateTimeString = response.Substring(7, 17);
                _utcStartTime =
                    DateTime.ParseExact(utcDateTimeString, "yy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                SyncTimeWithTimeManager();
                
            }
            catch
            {
                Debug.Log("Time has not set");
            }
        }

        private static async Task WrapErrors(this Task task)
        {
            await task;
        }
    }
}