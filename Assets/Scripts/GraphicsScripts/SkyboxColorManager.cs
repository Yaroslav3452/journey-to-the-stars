﻿using LevelManagerScripts;
using UnityEngine;

namespace GraphicsScripts
{
        public class SkyboxColorManager : MonoBehaviour
        {
                public static SkyboxColorManager Current;
                [SerializeField] private float changingSpeed;
                private int _colorUpID;
                private int _colorDownID;
                private int _noiseID;
                private bool _isChangingColor;

                public void SetNewNoiseColors()
                {
                        if (LevelsSwitcherSystem.Current.currentLevelManager != null)
                                Shader.SetGlobalTexture(_noiseID,
                                        LevelsSwitcherSystem.Current.currentLevelManager.skyboxSettingsSettings.noise);
                        _isChangingColor = true;
                }

                private void Update()
                {
                        if (!_isChangingColor) return;
                        var upGlobalColor = Shader.GetGlobalColor(_colorUpID);
                        var downGlobalColor = Shader.GetGlobalColor(_colorDownID);
                        if (LevelsSwitcherSystem.Current.currentLevelManager != null)
                        {
                                var deltaUp = Color.Lerp(upGlobalColor,
                                        LevelsSwitcherSystem.Current.currentLevelManager.skyboxSettingsSettings
                                                .upperColor,
                                        changingSpeed * Time.deltaTime);
                                var deltaDown = Color.Lerp(downGlobalColor,
                                        LevelsSwitcherSystem.Current.currentLevelManager.skyboxSettingsSettings
                                                .lowerColor,
                                        changingSpeed * Time.deltaTime);
                                Shader.SetGlobalColor(_colorUpID, deltaUp);
                                Shader.SetGlobalColor(_colorDownID, deltaDown);
                                if (deltaUp == LevelsSwitcherSystem.Current.currentLevelManager.skyboxSettingsSettings
                                            .upperColor) _isChangingColor = false;
                        }
                }

                private void Awake()
                {
                        Current = this;
                        _colorUpID = Shader.PropertyToID("_ColorUp");
                        _colorDownID = Shader.PropertyToID("_ColorDown");
                        _noiseID = Shader.PropertyToID("_NoiseTex");
                }

                private void Start()
                {
                        SetNewNoiseColors();
                        _isChangingColor = true;
                }
        }
}