﻿using UnityEngine;

public class SafeArea : MonoBehaviour
{
    RectTransform _panel;
    Rect _lastSafeArea = new Rect(0, 0, 0, 0);

    void Start()
    {
        _panel = GetComponent<RectTransform>();
        Refresh();
    }

    void Refresh()
    {
        Rect safeArea = GetSafeArea();

        if (safeArea != _lastSafeArea)
            ApplySafeArea(safeArea);
    }

    Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    void ApplySafeArea(Rect r)
    {
        _lastSafeArea = r;

        // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
        Vector2 anchorMin = r.position;
        Vector2 anchorMax = r.position + r.size;
        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        #if UNITY_EDITOR
        if (Screen.width == 1242 && Screen.height == 2688)
        {
            anchorMax.y = 0.96f;
        }
        #endif
        _panel.anchorMin = anchorMin;
        _panel.anchorMax = anchorMax;
    }
}