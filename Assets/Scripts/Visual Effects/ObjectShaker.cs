﻿using UnityEngine;

namespace Visual_Effects
{
    public class ObjectShaker : MonoBehaviour
    {
        [SerializeField] private Vector3 amplitude = new Vector3(2,2,2);
        [SerializeField] private float speed = 1f;
        [SerializeField] Transform upperTransform;
        [SerializeField] Transform lowerTransform;
        private Vector3 _targetPosition;
        private Vector3 _lowerPos;
        private Vector3 _upperPos;
        private Transform _transform;
        private Vector3 _defaultPosition;

        public ObjectShaker(Vector3 lowerPos)
        { 
            _lowerPos = lowerPos;
        }


        private void Update()
        {
            _transform.position = Vector3.MoveTowards(_transform.position, _targetPosition, speed * Time.deltaTime);
            if (_transform.position == _targetPosition)
            {
                if (_targetPosition == _lowerPos) _targetPosition = _upperPos;
                else if (_targetPosition == _upperPos) _targetPosition = _lowerPos;
            }
        }

        private void Start()
        {
            _transform = transform;
            _defaultPosition = _transform.position;
            if (lowerTransform == null && upperTransform == null)
            {
                _lowerPos = _defaultPosition + amplitude;
                _upperPos = _defaultPosition - amplitude;
            }
            else
            {
                _lowerPos = lowerTransform.position;
                _upperPos = upperTransform.position;
            }

            _targetPosition = _upperPos;
        }
    }
}