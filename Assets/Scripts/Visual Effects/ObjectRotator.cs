﻿using UnityEngine;

namespace Visual_Effects
{
    public class ObjectRotator : MonoBehaviour
    {
        [SerializeField] private Vector3 axisSpeed = Vector3.zero;
        private Transform _transform;
        private void Update()
        {
            _transform.Rotate(axisSpeed, Space.Self);
        }

        private void Start()
        {
            _transform = transform;
        }
    }
}

