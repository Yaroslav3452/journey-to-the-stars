﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]
public class GravityBody : MonoBehaviour {

	public GravityAttractor attractor;
	private Transform _myTransform;
	private Rigidbody _rigidbody;

	void Start () 
	{
		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		_rigidbody = GetComponent<Rigidbody>();
		_myTransform = transform;
	}

	void FixedUpdate () 
	{
		attractor.Attract(_myTransform, _rigidbody);
	}
}